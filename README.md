# Trucado

SNMP network device emulator for educational purposes. It will supports the bypassed operation *get (-g)* and *next (-n)* from **net-snmp** server.

And entry point will redirect the snmp request from **net-snmp** server to the corresponding API endpoint.

## Repository structure

Two main branchs, one **stable** *full working* and other feature branch called **dev** that not necessarily be in *operation or working state*.

## Project Status

At this moment, the project doesn't have any working release or image to be integrally tested. Any news will be reflected in this Readme.

### Badges

#### Pipelines

![stable](https://gitlab.com/linuxpizzacats/trucado/badges/stable/pipeline.svg?ignore_skipped=true&key_text=stable)

![dev](https://gitlab.com/linuxpizzacats/trucado/badges/dev/pipeline.svg?ignore_skipped=true&key_text=dev)

#### Coverage

![stable](https://gitlab.com/linuxpizzacats/trucado/badges/stable/coverage.svg?key_text=stable)

![dev](https://gitlab.com/linuxpizzacats/trucado/badges/dev/coverage.svg?key_text=dev)


#### Quality Gates on SonarCloud

**[stable](https://sonarcloud.io/dashboard?id=linuxpizzacats_trucado "Go to sonarcloud project's web")**

![stable QG Status](https://sonarcloud.io/api/project_badges/measure?project=linuxpizzacats_trucado&metric=alert_status "Quality Gate for stable branch") 

**[dev](https://sonarcloud.io/dashboard?id=linuxpizzacats_trucado&branch=dev "Go to sonarcloud project's web")**

![dev QG Status](https://sonarcloud.io/api/project_badges/measure?branch=dev&project=linuxpizzacats_trucado&metric=alert_status "Quality Gate for dev branch")

## About trucado project

This project uses Quarkus, the Supersonic Subatomic Java Framework. 

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

**We leave the [Quarkus readme](README-QUARKUS-APP.md) inside this repository to be used like a guide in case of needed it.**
