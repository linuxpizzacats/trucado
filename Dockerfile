# From debian 10
FROM debian:buster-slim

# folderapp creation
RUN mkdir /app

# For Java instalation
RUN mkdir -p /usr/share/man/man1 /usr/share/man/man2

# Update repos and install net-snmp 
RUN apt update && apt install -y snmpd openjdk-11-jre-headless curl
WORKDIR /app/
COPY . .
WORKDIR /app/target/quarkus-app/
CMD ["/bin/bash","-c","snmpd -C -c ../classes/snmpd.conf && java -jar quarkus-run.jar"]
# Trucado and SNMPD container port
EXPOSE 8080 161/udp
