#!/bin/bash
TOTAL=0
TOTAL_COVERED=0

for line in $(grep -v METH ./target/site/jacoco/jacoco.csv); 
do 
  MISSED=$(echo $line | cut -d "," -f4); 
  COVERED=$(echo $line | cut -d "," -f5); 
  TOTAL=$(( TOTAL + ( MISSED + COVERED )));
  TOTAL_COVERED=$(( TOTAL_COVERED + COVERED ));
done

echo "JaCoCo Results from jacoco.csv"
echo "Coverage => $((( TOTAL_COVERED * 100 ) / TOTAL )) %"
exit 0
