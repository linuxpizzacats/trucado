package org.lpc.repositories;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.lpc.models.Item;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.repositories.ItemRepository;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Item Repository")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemRepositoryTest {
	@Inject
	ItemRepository repository;
	
	@Test
	void Getting_an_item() throws ItemNotFoundException{
		//Given
		Item item=repository.findById(1);
		
		//When
		
		
		//Then
		Assertions.assertEquals(1,item.getId());
	}
	
	@Test
	void Trying_to_get_an_non_existing_item_throws_ItemNotFoundException() {
		//Given	
		long nonExistingId=0;
		
		//When
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
								() -> {
									repository.findById(nonExistingId);
							    });
		
		
		//Then
			
		Assertions.assertEquals("The item with id '"+nonExistingId+"' does not exist.", exception.getMessage());		
	}	
	
	@Test
	void Creating_a_new_item_and_save_it() {
		//Given
		Item item=new Item();
		item.setOid(".1.3.6.1.2.1.2.2.1.1.1.999999");
		
		//When
		repository.create(item);
		
		//Then
		Assertions.assertEquals(".1.3.6.1.2.1.2.2.1.1.1.999999",item.getOid());
		Assertions.assertTrue(item.getId()>0);
		repository.delete(item);
	}
	
	@Test
	void Updating_an_item() throws ItemNotFoundException {
		//Given
		Item item=repository.findById(1);
		String oidOriginal=item.getOid();
		item.setOid(".1.3.6.1.2.1.2.2.1.1.1.999999");
		
		//When
		repository.update(item);
		item=repository.findById(1);
		
		//Then
		Assertions.assertEquals(".1.3.6.1.2.1.2.2.1.1.1.999999",item.getOid());
		Assertions.assertEquals(1,item.getId());
		
		item.setOid(oidOriginal);
		repository.update(item);
	}
	
	@Test
	void Deleting_an_item() {
		//Given
		Item item=new Item();
		item.setOid(".1.3.6.1.2.1.2.2.1.1.1.999999");		
		item=repository.create(item);
		long id=item.getId();
		
		//When
		repository.delete(item);
		
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					repository.findById(id);
			    });
		//Then
			
		Assertions.assertEquals("The item with id '"+id+"' does not exist.", exception.getMessage());	
	}

	@Test
	void Find_an_existent_Item_by_its_exact_oid() throws ItemNotFoundException {
		//Given
		String oid=".1.3.6.1.2.1.1.1.0"; //sysDescr oid from SYSTEM MIB
				
		
				
				
		//When
		Item item=repository.findByOid(oid);	
				
		
		//Then IllegalArgumentException
		Assertions.assertEquals(oid, item.getOid());
		
	}	
	@Test
	void Trying_to_get_an_non_existing_Item_by_its_exact_oid_throws_ItemNotFoundException() throws ItemNotFoundException {
		//Given
		String noExistingOid=".1.3.6.1.2.1.1.9999.0"; //sysDescr oid from SYSTEM MIB
				
		
				
				
		//When
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
								() -> {
									repository.findByOid(noExistingOid);
							    });
		
		
		//Then
			
		Assertions.assertEquals("The item with oid '"+noExistingOid+"' does not exist.", exception.getMessage());
		
	}
	
	@Test
	void Find_an_existent_Item_by_part_of_its_oid() throws ItemNotFoundException {
		//Given
		String incompleteOid=".1.3.6.1.2.1.1.1"; 
		String fullOid=".1.3.6.1.2.1.1.1.0";//sysDescr oid from SYSTEM MIB
		
				
				
		//When
		Item item=repository.findByOidLike(incompleteOid);	
				
		
		//Then IllegalArgumentException
		Assertions.assertEquals(fullOid, item.getOid());
		
	}	
	@Test
	void Trying_to_find_an_unexistent_Item_by_part_of_its_oid_throws_ItemNotFoundException() throws ItemNotFoundException {
		//Given
		String noExistingOid=".1.3.6.1.2.1.1.9999.0"; //sysDescr oid from SYSTEM MIB
				
		
				
				
		//When
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
								() -> {
									repository.findByOidLike(noExistingOid);
							    });
		
		
		//Then
			
		Assertions.assertEquals("The item with oid '"+noExistingOid+"' does not exist.", exception.getMessage());
		
	}	
}
