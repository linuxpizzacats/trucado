package org.lpc.repositories;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.lpc.models.Interface;
import org.lpc.models.Item;
import org.lpc.models.exceptions.InterfaceNotFoundException;
import org.lpc.models.exceptions.ItemNotFoundException;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Interface Repository")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InterfaceRepositoryTest {
	@Inject
	InterfaceRepository repository;
	
	@Inject
	ItemRepository itemRepository;
	
	@Test
	void Getting_an_interface() throws InterfaceNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		
		//When
		
		//Then
		Assertions.assertEquals(1, interfaz.getId());
	}
	
	@Test
	void Trying_to_get_an_non_existing_item_throws_ItemNotFoundException() {
		//Given	
		int nonExistingId=0;
		
		//When
		Exception exception = Assertions.assertThrows(InterfaceNotFoundException.class, 
								() -> {
									repository.findById(nonExistingId);
							    });
		
		
		//Then
			
		Assertions.assertEquals("The interface with id '"+nonExistingId+"' does not exist.", exception.getMessage());		
	}
	
	@Test
	void Creating_a_new_interface_and_saving_it() {
		//Given
		Interface interfaz=new Interface();
		interfaz.setAdminStatus(new Item());
		interfaz.setAlias(new Item());
		interfaz.setHcInOctects(new Item());
		interfaz.setHcOutOctects(new Item());
		interfaz.setHighspeed(new Item());
		interfaz.setIndex(new Item());
		interfaz.setInDiscards(new Item());
		interfaz.setInErrors(new Item());
		interfaz.setName(new Item());
		interfaz.setOperStatus(new Item());
		interfaz.setOutDiscards(new Item());
		interfaz.setOutErrors(new Item());
		
		//When
		repository.create(interfaz);
		
		//Then
		Assertions.assertTrue(interfaz.getId()>0);
	}

	@Test
	void Updating_an_interface_adminStatus() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getAdminStatus().getId();
		
		interfaz.setAdminStatus(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getAdminStatus().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	
	@Test
	void Updating_an_interface_operStatus() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getOperStatus().getId();
		
		interfaz.setOperStatus(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getOperStatus().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}
	@Test
	void Updating_an_interface_alias() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getAlias().getId();
		
		interfaz.setAlias(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getAlias().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_hcInOctects() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getHcInOctects().getId();
		
		interfaz.setHcInOctects(new Item());
		interfaz.getHcInOctects().setOid(".1.3.6.1.2.1.31.1.1.1.6.1");
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getHcInOctects().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}
	@Test
	void Updating_an_interface_hcOutOctects() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getHcOutOctects().getId();
		
		interfaz.setHcOutOctects(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getHcOutOctects().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_highspeed() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getHighspeed().getId();
		
		interfaz.setHighspeed(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getHighspeed().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}
	@Test
	void Updating_an_interface_index() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getIndex().getId();
		
		interfaz.setIndex(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getIndex().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_inDiscards() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getInDiscards().getId();
		
		interfaz.setInDiscards(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getInDiscards().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_inErrors() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getInErrors().getId();
		
		interfaz.setInErrors(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getInErrors().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_name() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getName().getId();
		
		interfaz.setName(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getName().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_outDiscards() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getOutDiscards().getId();
		
		interfaz.setOutDiscards(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getOutDiscards().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Updating_an_interface_outErrors() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= repository.findById(1);
		long previousId=interfaz.getOutErrors().getId();
		
		interfaz.setOutErrors(new Item());
				
		//When
		repository.update(interfaz);
		interfaz=repository.findById(1);
		long newId=interfaz.getOutErrors().getId();
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					itemRepository.findById(previousId);
			    });		
		
		//Then
		Assertions.assertEquals(1,interfaz.getId());
		Assertions.assertNotEquals(previousId, newId);
		Assertions.assertEquals("The item with id '"+previousId+"' does not exist.", exception.getMessage());

	}	
	@Test
	void Deleting_an_interface() throws InterfaceNotFoundException, ItemNotFoundException {
		//Given
		Interface interfaz= new Interface();
				
		interfaz=repository.create(interfaz);
		long id=interfaz.getId();
		
		
				
		//When
		repository.delete(interfaz);
		Exception exception = Assertions.assertThrows(InterfaceNotFoundException.class, 
				() -> {
					repository.findById(id);
			    });		
		
		//Then
		Assertions.assertEquals("The interface with id '"+id+"' does not exist.", exception.getMessage());

	}	
}
