package org.lpc.models;

import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.string.ExhaustedStrategy;
import org.lpc.models.strategies.items.string.FixedAtMaxStrategy;
import org.lpc.models.strategies.items.string.FixedAtMinStrategy;
import org.lpc.models.strategies.items.string.FixedAtStrategy;
import org.lpc.models.strategies.items.string.FlappingStrategy;
import org.lpc.models.strategies.items.string.RandomStrategy;
import org.lpc.models.strategies.items.string.UsageAtPercentStrategy;
import org.lpc.models.strategies.items.string.ZeroStrategy;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Item String")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemStringTest {
	
	Item item=new Item(ItemType.STRING);
	
	
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
	class Item_String_Type_Strategies_tests {
    	
		@Test
		void Item_calculated_value_is_zero_if_ZeroStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals("0", calculated);
		}
		
		@Test
		void Item_calculated_value_is_foo_or_bar_if_FlappingStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FlappingStrategy());
			item.setAux("foo,bar");
			
			//When
			String calculated= item.calculate();
			
			//Then			
			Assertions.assertTrue(Arrays.asList(item.getAux().split(",")).contains(calculated));
		}

		@Test
		void Item_calculated_value_is_same_as_actual_value_if_ExhaustedStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ExhaustedStrategy());
			item.setValue("actual value");
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getValue(), calculated);
		}
		
		@DisplayName("Item calculated value must be equals to actual value because UsageAtPercentStrategy is used on string type")
        @ParameterizedTest(name = "Usage at {0}% so value must be \"actual value\".")
        @ValueSource(strings = { "25", "50", "75", "100" })
		void Item_String_type_calculated_value_is_actual_value_because_UsageAtPercentStrategy(String percentage) throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setValue("actual value");
			item.setMax("100");
			item.setMin("0");
			item.setStrategy(new UsageAtPercentStrategy());
			item.setAux(percentage);

			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals("actual value",calculated);
		}
		
		@DisplayName("Item calculated value must be equals to wanted value because FixedAtStrategy is used")
        @ParameterizedTest(name = "Fixed at \"{0}\" so value must be \"{0}\".")
        @ValueSource(strings = { "foo","29", "58", "bar", "GNU/Linux", "something something" })
		void Item_calculated_value_is_x_because_if_FixedAtStrategy_is_used_with_x_value_wanted(String aux) throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setAux(aux);
			item.setStrategy(new FixedAtStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(aux,calculated);
		}	
		
		@Test
		void Item_calculated_value_is_min_because_FixedAtMinStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMinStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMin(),calculated);
		}
		
		@Test
		void Item_calculated_value_is_max_because_FixedAtMaxStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMaxStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMax(),calculated);
		}
		
		
		@Test
		void Item_calculated_value_is_a_random_string_because_RandomStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new RandomStrategy());
			item.setValue("actual value");
			
			//When
			String calculatedValue= item.calculate();

			//Then
			Assertions.assertNotEquals("actual value", calculatedValue); 
		}
    }
    
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    class Item_String_General_tests {
    	    	
		@Test
		void Item_value_is_equals_to_calculated_value() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			item.setValue("old value");
			Assertions.assertEquals("old value", item.getValue());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(calculated, item.getValue());
		}
	
		@Test
		void Item_previous_value_is_equals_to_initial_item_value_after_a_new_one_valid_calculation() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			item.setValue("old value");
			item.setPreviousValue("previous value");
			Assertions.assertEquals("previous value", item.getPreviousValue());
			
			//When
			item.calculate();
			
			//Then
			Assertions.assertEquals("old value", item.getPreviousValue());
		}

    }
}
