package org.lpc.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.lpc.models.commands.interfaces.AdminDown;
import org.lpc.models.commands.interfaces.AdminUp;
import org.lpc.models.commands.interfaces.Flapping;
import org.lpc.models.commands.interfaces.OperDown;
import org.lpc.models.commands.interfaces.OperUp;
import org.lpc.models.commands.interfaces.UsageAt;
import org.lpc.models.commands.interfaces.UsageInAt;
import org.lpc.models.commands.interfaces.UsageInNone;
import org.lpc.models.commands.interfaces.UsageNone;
import org.lpc.models.commands.interfaces.UsageOutAt;
import org.lpc.models.commands.interfaces.UsageOutNone;
import org.lpc.models.commands.interfaces.WithDiscards;
import org.lpc.models.commands.interfaces.WithDiscardsIn;
import org.lpc.models.commands.interfaces.WithDiscardsOut;
import org.lpc.models.commands.interfaces.WithErrors;
import org.lpc.models.commands.interfaces.WithErrorsAndDiscards;
import org.lpc.models.commands.interfaces.WithErrorsAndDiscardsIn;
import org.lpc.models.commands.interfaces.WithErrorsAndDiscardsOut;
import org.lpc.models.commands.interfaces.WithErrorsIn;
import org.lpc.models.commands.interfaces.WithErrorsOut;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.ZeroStrategy;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Interface")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InterfaceTest {
	
	Interface interfaz;
	
	@BeforeEach
	void setup() {
		interfaz=new Interface();
	}
	
	@Test
	void Interface_with_flapping_behaviour_has_operstatus_item_values_varying_between_one_and_two() throws WrongItemTypeForSelectedStrategyException {
		//Given
		Item item=interfaz.getOperStatus();
		item.setStrategy(new ZeroStrategy());
		String calculatedValue=item.calculate();
		Assertions.assertEquals("0",calculatedValue);
		
		//When
		interfaz.apply(new Flapping());
		calculatedValue=item.calculate();
				
		//Then
		Assertions.assertTrue(calculatedValue.equals("1") || calculatedValue.equals("2"));
		
	}
	
	@Test
	void Interface_with_operational_status_down_behaviour_has_operstatus_item_value_fixed_at_two() throws WrongItemTypeForSelectedStrategyException {
		//Given
		Item item=interfaz.getOperStatus();
		item.setStrategy(new ZeroStrategy());
		String calculatedValue=item.calculate();
		Assertions.assertEquals("0",calculatedValue);
		
		//When
		interfaz.apply(new OperDown());
		calculatedValue=item.calculate();
				
		//Then
		Assertions.assertEquals("2",calculatedValue);
		
	}

	@Test
	void Interface_with_operational_status_up_behaviour_has_operstatus_item_value_fixed_at_one() throws WrongItemTypeForSelectedStrategyException {
		//Given
		Item item=interfaz.getOperStatus();
		item.setStrategy(new ZeroStrategy());
		String calculatedValue=item.calculate();
		Assertions.assertEquals("0",calculatedValue);
		
		//When
		interfaz.apply(new OperUp());
		calculatedValue=item.calculate();
				
		//Then
		Assertions.assertEquals("1",calculatedValue);
		
	}

	@Test
	void Interface_with_admin_status_down_behaviour_has_operstatus_and_adminstatus_values_fixed_to_two() throws WrongItemTypeForSelectedStrategyException {
		//Given
		interfaz.getOperStatus().setStrategy(new ZeroStrategy());
		interfaz.getAdminStatus().setStrategy(new ZeroStrategy());
		
		//When
		interfaz.apply(new AdminDown());
				
		//Then
		Assertions.assertEquals("2",interfaz.getOperStatus().calculate());
		Assertions.assertEquals("2",interfaz.getAdminStatus().calculate());
		
	}	
	@Test
	void Interface_with_admin_status_up_behaviour_has_operstatus_and_adminstatus_values_fixed_to_one() throws WrongItemTypeForSelectedStrategyException {
		//Given
		interfaz.getOperStatus().setStrategy(new ZeroStrategy());
		interfaz.getAdminStatus().setStrategy(new ZeroStrategy());
		
		//When
		interfaz.apply(new AdminUp());
				
		//Then
		Assertions.assertEquals("1",interfaz.getOperStatus().calculate());
		Assertions.assertEquals("1",interfaz.getAdminStatus().calculate());
		
	}	
	
	@Test
	void Interface_with_discards_in_behaviour_has_inDiscards_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getInDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInDiscards().setValue("0");
		interfaz.getInDiscards().setAux("10000");
		interfaz.getInDiscards().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new WithDiscardsIn());
		long calculatedValue= Long.parseLong(interfaz.getInDiscards().calculate());

		//Then
		Assertions.assertTrue(calculatedValue>=0 && calculatedValue < 10000);
		
	}
	
	@Test
	void Interface_with_discards_out_behaviour_has_outDiscards_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getOutDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutDiscards().setValue("0");
		interfaz.getOutDiscards().setAux("10000");
		interfaz.getOutDiscards().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new WithDiscardsOut());
		long calculatedValue= Long.parseLong(interfaz.getOutDiscards().calculate());

		//Then
		Assertions.assertTrue(calculatedValue>=0 && calculatedValue < 10000);
		
	}
	@Test
	void Interface_with_discards_behaviour_has_outDiscards_and_inDiscardas_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getOutDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutDiscards().setValue("0");
		interfaz.getOutDiscards().setAux("10000");
		interfaz.getOutDiscards().setTsLastQuery(pastTs);//1 second to the past
		interfaz.getInDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInDiscards().setValue("0");
		interfaz.getInDiscards().setAux("10000");
		interfaz.getInDiscards().setTsLastQuery(pastTs);//1 second to the past		
		//When
		interfaz.apply(new WithDiscards());
		long calculatedValueOut= Long.parseLong(interfaz.getOutDiscards().calculate());
		long calculatedValueIn= Long.parseLong(interfaz.getInDiscards().calculate());

		//Then
		Assertions.assertTrue(calculatedValueOut>=0 && calculatedValueOut < 10000);
		Assertions.assertTrue(calculatedValueIn>=0 && calculatedValueIn < 10000);
		
	}
	
	@Test
	void Interface_with_errors_in_behaviour_has_inDiscards_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getInErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInErrors().setValue("0");
		interfaz.getInErrors().setAux("10000");
		interfaz.getInErrors().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new WithErrorsIn());
		long calculatedValue= Long.parseLong(interfaz.getInErrors().calculate());

		//Then
		Assertions.assertTrue(calculatedValue>=0 && calculatedValue < 10000);
		
	}	
	@Test
	void Interface_with_errors_out_behaviour_has_outErrors_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getOutErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutErrors().setValue("0");
		interfaz.getOutErrors().setAux("10000");
		interfaz.getOutErrors().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new WithErrorsOut());
		long calculatedValue= Long.parseLong(interfaz.getOutErrors().calculate());

		//Then
		Assertions.assertTrue(calculatedValue>=0 && calculatedValue < 10000);
		
	}
	@Test
	void Interface_with_errors_behaviour_has_outErrors_and_inErrors_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getOutErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutErrors().setValue("0");
		interfaz.getOutErrors().setAux("10000");
		interfaz.getOutErrors().setTsLastQuery(pastTs);//1 second to the past
		interfaz.getInErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInErrors().setValue("0");
		interfaz.getInErrors().setAux("10000");
		interfaz.getInErrors().setTsLastQuery(pastTs);//1 second to the past		
		//When
		interfaz.apply(new WithErrors());
		long calculatedValueOut= Long.parseLong(interfaz.getOutErrors().calculate());
		long calculatedValueIn= Long.parseLong(interfaz.getInErrors().calculate());

		//Then
		Assertions.assertTrue(calculatedValueOut>=0 && calculatedValueOut < 10000);
		Assertions.assertTrue(calculatedValueIn>=0 && calculatedValueIn < 10000);
		
	}	
	
	@Test
	void Interface_with_discards_and_errors_behaviour_has_both_in_and_out_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getOutDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutDiscards().setValue("0");
		interfaz.getOutDiscards().setAux("10000");
		interfaz.getOutDiscards().setTsLastQuery(pastTs);//1 second to the past
		interfaz.getInDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInDiscards().setValue("0");
		interfaz.getInDiscards().setAux("10000");
		interfaz.getInDiscards().setTsLastQuery(pastTs);//1 second to the past	
		interfaz.getOutErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutErrors().setValue("0");
		interfaz.getOutErrors().setAux("10000");
		interfaz.getOutErrors().setTsLastQuery(pastTs);//1 second to the past
		interfaz.getInErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInErrors().setValue("0");
		interfaz.getInErrors().setAux("10000");
		interfaz.getInErrors().setTsLastQuery(pastTs);//1 second to the past			
		
		//When
		interfaz.apply(new WithErrorsAndDiscards());
		long calculatedValueDiscardsOut= Long.parseLong(interfaz.getOutDiscards().calculate());
		long calculatedValueDiscardsIn= Long.parseLong(interfaz.getInDiscards().calculate());
		long calculatedValueErrorsOut= Long.parseLong(interfaz.getOutErrors().calculate());
		long calculatedValueErrorsIn= Long.parseLong(interfaz.getInErrors().calculate());
		//Then
		Assertions.assertTrue(calculatedValueDiscardsOut>=0 && calculatedValueDiscardsOut < 10000);
		Assertions.assertTrue(calculatedValueDiscardsIn>=0 && calculatedValueDiscardsIn < 10000);
		Assertions.assertTrue(calculatedValueErrorsOut>=0 && calculatedValueErrorsOut < 10000);
		Assertions.assertTrue(calculatedValueErrorsIn>=0 && calculatedValueErrorsIn < 10000);
	}	
	
	@Test
	void Interface_with_in_discards_and_errors_behaviour_in_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;

		interfaz.getInDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInDiscards().setValue("0");
		interfaz.getInDiscards().setAux("10000");
		interfaz.getInDiscards().setTsLastQuery(pastTs);//1 second to the past	

		interfaz.getInErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getInErrors().setValue("0");
		interfaz.getInErrors().setAux("10000");
		interfaz.getInErrors().setTsLastQuery(pastTs);//1 second to the past			
		
		//When
		interfaz.apply(new WithErrorsAndDiscardsIn());
		
		long calculatedValueDiscardsIn= Long.parseLong(interfaz.getInDiscards().calculate());
		
		long calculatedValueErrorsIn= Long.parseLong(interfaz.getInErrors().calculate());
		//Then
		Assertions.assertTrue(calculatedValueDiscardsIn>=0 && calculatedValueDiscardsIn < 10000);
		Assertions.assertTrue(calculatedValueErrorsIn>=0 && calculatedValueErrorsIn < 10000);
	}
	
	@Test
	void Interface_with_out_discards_and_errors_behaviour_has_out_values_random_with_limit_max() throws WrongItemTypeForSelectedStrategyException {
		//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		
		interfaz.getOutDiscards().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutDiscards().setValue("0");
		interfaz.getOutDiscards().setAux("10000");
		interfaz.getOutDiscards().setTsLastQuery(pastTs);//1 second to the past
		
		interfaz.getOutErrors().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getOutErrors().setValue("0");
		interfaz.getOutErrors().setAux("10000");
		interfaz.getOutErrors().setTsLastQuery(pastTs);//1 second to the past
		
		
		//When
		interfaz.apply(new WithErrorsAndDiscardsOut());
		long calculatedValueDiscardsOut= Long.parseLong(interfaz.getOutDiscards().calculate());
		long calculatedValueErrorsOut= Long.parseLong(interfaz.getOutErrors().calculate());
		//Then
		Assertions.assertTrue(calculatedValueDiscardsOut>=0 && calculatedValueDiscardsOut < 10000);
		Assertions.assertTrue(calculatedValueErrorsOut>=0 && calculatedValueErrorsOut < 10000);
	}	
	
	
    @ParameterizedTest(name = "Usage in at {0}%")
    @ValueSource(strings = { "25", "50", "75", "100" })
	void Interface_with_usage_in_at_some_percentage_behaviour_has_inhcoctects_item_values_at_that_desired_value(String percentage) throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getHcInOctects().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getHcInOctects().setValue("0");
		interfaz.getHcInOctects().setAux("{ \"limit\": \"10000\", \"percentage\": \""+percentage+"\"}");
		interfaz.getHcInOctects().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new UsageInAt());
		long calculatedValue= Long.parseLong(interfaz.getHcInOctects().calculate());
		long desiredValue=(10000*Long.parseLong(percentage))/100;
		//Then
		Assertions.assertEquals(desiredValue,calculatedValue);
		
	}

    @ParameterizedTest(name = "Usage out at {0}%")
    @ValueSource(strings = { "25", "50", "75", "100" })
	void Interface_with_usage_out_at_some_percentage_behaviour_has_outhcoctects_item_values_at_that_desired_value(String percentage) throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getHcOutOctects().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getHcOutOctects().setValue("0");
		interfaz.getHcOutOctects().setAux("{ \"limit\": \"10000\", \"percentage\": \""+percentage+"\"}");
		interfaz.getHcOutOctects().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new UsageOutAt());
		long calculatedValue= Long.parseLong(interfaz.getHcOutOctects().calculate());
		long desiredValue=(10000*Long.parseLong(percentage))/100;
		//Then
		Assertions.assertEquals(desiredValue,calculatedValue);
		
	}
    
    @ParameterizedTest(name = "Usage at {0}%")
    @ValueSource(strings = { "25", "50", "75", "100" })
	void Interface_with_usage_at_some_percentage_behaviour_has_out_and_in_item_values_at_that_desired_value(String percentage) throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		long actualTs=System.currentTimeMillis();
		long pastTs=actualTs-1000;
		interfaz.getHcOutOctects().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getHcOutOctects().setValue("0");
		interfaz.getHcOutOctects().setAux("{ \"limit\": \"10000\", \"percentage\": \""+percentage+"\"}");
		interfaz.getHcOutOctects().setTsLastQuery(pastTs);//1 second to the past
		interfaz.getHcInOctects().setStrategy(new org.lpc.models.strategies.items.counter.ZeroStrategy());
		interfaz.getHcInOctects().setValue("0");
		interfaz.getHcInOctects().setAux("{ \"limit\": \"10000\", \"percentage\": \""+percentage+"\"}");
		interfaz.getHcInOctects().setTsLastQuery(pastTs);//1 second to the past
		
		//When
		interfaz.apply(new UsageAt());
		long calculatedValueOut= Long.parseLong(interfaz.getHcOutOctects().calculate());
		long calculatedValueIn= Long.parseLong(interfaz.getHcInOctects().calculate());
		long desiredValue=(10000*Long.parseLong(percentage))/100;
		
		//Then
		Assertions.assertEquals(desiredValue,calculatedValueOut);
		Assertions.assertEquals(desiredValue,calculatedValueIn);
		
	}
    
    @Test
	void Interface_with_usage_in_at_none_behaviour_has_inhcoctects_item_values_at_zero() throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		
		//When
		interfaz.apply(new UsageInNone());
		
		//Then
		Assertions.assertEquals("0",interfaz.getHcInOctects().calculate());
		
	}    
    @Test
	void Interface_with_usage_out_at_none_behaviour_has_outhcoctects_item_values_at_zero() throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		
		//When
		interfaz.apply(new UsageOutNone());
		
		//Then
		Assertions.assertEquals("0",interfaz.getHcOutOctects().calculate());
		
	}     
    @Test
	void Interface_with_usage_none_behaviour_has_in_and_out_item_values_at_zero() throws WrongItemTypeForSelectedStrategyException {
		//Given
		
		//mocking UsageAtPercentStrategy.getTimestamp()
    	//Given
		
		//When
		interfaz.apply(new UsageNone());
		
		
		//Then
		Assertions.assertEquals("0",interfaz.getHcOutOctects().calculate());
		Assertions.assertEquals("0",interfaz.getHcInOctects().calculate());
	}     
}
