package org.lpc.models;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.ExhaustedStrategy;
import org.lpc.models.strategies.items.gauge.FixedAtMaxStrategy;
import org.lpc.models.strategies.items.gauge.FixedAtMinStrategy;
import org.lpc.models.strategies.items.gauge.FixedAtStrategy;
import org.lpc.models.strategies.items.gauge.FlappingStrategy;
import org.lpc.models.strategies.items.gauge.RandomStrategy;
import org.lpc.models.strategies.items.gauge.UsageAtPercentStrategy;
import org.lpc.models.strategies.items.gauge.ZeroStrategy;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Item Gauge")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemGaugeTest {
	
	Item item=new Item(ItemType.GAUGE);
		
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
	class Item_Gauge_Type_Strategies_tests {
    	
		@Test
		void Item_calculated_value_is_zero_if_ZeroStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals("0", calculated);
		}
		
		@Test
		void Item_calculated_value_is_one_or_two_if_FlappingStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FlappingStrategy());
			item.setAux("1,2");
			
			//When
			String calculated= item.calculate();
			
			//Then			
			Assertions.assertTrue(Arrays.asList(item.getAux().split(",")).contains(calculated));
		}

		@Test
		void Item_calculated_value_is_close_to_max_if_ExhaustedStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ExhaustedStrategy());
			item.setMax("100");
			item.setMin("0");
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertTrue(Integer.parseInt(calculated)>=(Integer.parseInt(item.getMax()) * 0.98) &&
									Integer.parseInt(calculated)<=Integer.parseInt(item.getMax())
					);
		}
		
		@DisplayName("Item calculated value must be equals to wanted % because UsageAtPercentStrategy is used")
        @ParameterizedTest(name = "Usage at {0}% so value must be {0}.")
        @ValueSource(strings = { "25", "50", "75", "100" })
		void Item_calculated_value_is_x_because_if_UsageAtPercentStrategy_is_used_with_x_percent_wanted(String percentage) throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setMax("100");
			item.setMin("0");
			item.setStrategy(new UsageAtPercentStrategy());
			item.setAux(percentage);

			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(percentage,calculated);
		}
		
		@DisplayName("Item calculated value must be equals to wanted value because FixedAtStrategy is used")
        @ParameterizedTest(name = "Fixed at \"{0}\" so value must be \"{0}\".")
        @ValueSource(strings = { "foo","29", "58", "bar", "GNU/Linux", "something something" })
		void Item_calculated_value_is_x_because_if_FixedAtStrategy_is_used_with_x_value_wanted(String aux) throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setAux(aux);
			item.setStrategy(new FixedAtStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(aux,calculated);
		}	
		
		@Test
		void Item_calculated_value_is_min_because_FixedAtMinStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMinStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMin(),calculated);
		}
		
		@Test
		void Item_calculated_value_is_max_because_FixedAtMaxStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMaxStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMax(),calculated);
		}
		
		
		@Test
		void Item_calculated_value_is_betwen_max_and_min_because_RandomStrategy_is_used() throws NumberFormatException, WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new RandomStrategy());
			item.setMax("75");
			item.setMin("10");
			
			//When
			int calculatedValue= Integer.parseInt(item.calculate());
			
			//Then
			Assertions.assertTrue(calculatedValue <= Integer.parseInt(item.getMax()) && 
									calculatedValue>=Integer.parseInt(item.getMin()));
		}
    }
    
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    class Item_Gauge_General_tests {
    	    	
		@Test
		void Item_value_is_equals_to_calculated_value() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			item.setValue("old value");
			Assertions.assertEquals("old value", item.getValue());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(calculated, item.getValue());
		}
	
		@Test
		void Item_previous_value_is_equals_to_initial_item_value_after_a_new_one_calculation() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			item.setValue("old value");
			item.setPreviousValue("previous value");
			Assertions.assertEquals("previous value", item.getPreviousValue());
			
			//When
			item.calculate();
			
			//Then
			Assertions.assertEquals("old value", item.getPreviousValue());
		}
    }
}
