package org.lpc.models;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.counter.ExhaustedStrategy;
import org.lpc.models.strategies.items.counter.FixedAtMaxStrategy;
import org.lpc.models.strategies.items.counter.FixedAtMinStrategy;
import org.lpc.models.strategies.items.counter.FixedAtStrategy;
import org.lpc.models.strategies.items.counter.FlappingStrategy;
import org.lpc.models.strategies.items.counter.RandomStrategy;
import org.lpc.models.strategies.items.counter.UsageAtPercentStrategy;
import org.lpc.models.strategies.items.counter.ZeroStrategy;
import org.mockito.Mockito;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Item Counter")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemCounterTest {
	Item item=new Item(ItemType.COUNTER);
	
	
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
	class Item_Counter_Type_Strategies_tests {
    	
		@Test
		void Item_calculated_value_is_zero_if_ZeroStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new ZeroStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals("0", calculated);
		}
		
		@Test
		void Item_calculated_value_is_same_as_actual_value_if_FlappingStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FlappingStrategy());
			String actualValue=item.getValue();
			
			//When
			String calculated= item.calculate();
			
			//Then			
			Assertions.assertEquals(actualValue,calculated);
		}

		@Test
		void Item_calculated_value_is_close_to_change_per_second_given_limit_and_last_ts_if_ExhaustedStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			/*
			 * 	Change per second on an 10Mbps interface simulation trough 5 minutes (300 seconds)
			 * 	Min and Max bounds are given by the SNMP 64 bits counter definition or Long Type variable for Java aproach
			 *  Aux value is same as 10Mbps Highspeed's interface but in octects because that's the way snmp counter traffic works. 
			 *  The variation of counter between 1 second cannot be bigger than Aux value.  
			 */
			
			//mocking ExhaustedStrategy.getTimestamp()
			long actualTs=System.currentTimeMillis();
	        ExhaustedStrategy mockedStrategy = Mockito.mock(ExhaustedStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
			item.setStrategy(mockedStrategy);
			item.setValue("0");
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("10000000");//Limit
        
			// Travel 5 minutes to the pass
			item.setTsLastQuery(actualTs-300000);
			long diffInSeconds=300; //5 minutes
			long max=Long.parseLong(item.getValue())+ (diffInSeconds*Long.parseLong(item.getAux()));
	        Mockito.when(mockedStrategy.getMax(item, (diffInSeconds*Long.parseLong(item.getAux())))).thenCallRealMethod();
			long low=(long)(max*0.98);				
			
			//When
			String calculated= item.calculate();

			//Then
			Assertions.assertTrue(Long.parseLong(calculated)>=low &&
									Long.parseLong(calculated)<=max
					);
		}
		
        @ParameterizedTest(name = "Usage at {0}%")
        @ValueSource(strings = { "25", "50", "75", "100" })
		void Item_String_type_calculated_value_is_actual_value_because_UsageAtPercentStrategy(String percentage) throws WrongItemTypeForSelectedStrategyException {
			//Given
			/*
			 *  The variation of counter must be the defined percentage of Limit. Due to Limit is the maximum counter 
			 *  increase. 
			 *  This item configuration requires the following data in Aux field:
			 *  	- Limit
			 *  	- Percentage
			 *  Example:
			 *  	Aux = "{ Limit: 10000000, Percentage: 25}"
			 */
			
			
			//mocking UsageAtPercentStrategy.getTimestamp()
			long actualTs=System.currentTimeMillis();
	        UsageAtPercentStrategy mockedStrategy = Mockito.mock(UsageAtPercentStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
	        long limit=10000000;
			item.setStrategy(mockedStrategy);
			item.setValue("0");
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("{ \"limit\": \""+ limit +"\", \"percentage\": \""+percentage+"\"}");
			// Travel 5 minutes to the pass
			item.setTsLastQuery(actualTs-300000);
			long diffInSeconds=300; //5 minutes
			long expectedValue=(long)(Long.parseLong(percentage)*(diffInSeconds*limit)/100);
			Mockito.when(mockedStrategy.getMax(item, diffInSeconds*limit)).thenCallRealMethod();
			//When
			String calculated= item.calculate();

			//Then
			Assertions.assertEquals(expectedValue,Long.parseLong(calculated));
		}
		
		@DisplayName("Item calculated value must be equals to wanted value because FixedAtStrategy is used")
        @ParameterizedTest(name = "Fixed at \"{0}\" so value must be \"{0}\".")
        @ValueSource(strings = { "29", "58", "10000000" })
		void Item_calculated_value_is_x_because_if_FixedAtStrategy_is_used_with_x_value_wanted(String aux) throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setAux(aux);
			item.setStrategy(new FixedAtStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(aux,calculated);
		}	
		
		@Test
		void Item_calculated_value_is_min_because_FixedAtMinStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMinStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMin(),calculated);
		}
		
		@Test
		void Item_calculated_value_is_max_because_FixedAtMaxStrategy_is_used() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setStrategy(new FixedAtMaxStrategy());
			
			//When
			String calculated= item.calculate();
			
			//Then
			Assertions.assertEquals(item.getMax(),calculated);
		}
		
		
		@Test
		void Item_calculated_value_is_betwen_zero_and_limit_because_RandomStrategy_is_used() throws NumberFormatException, WrongItemTypeForSelectedStrategyException {
			//Given
			
			/*
			 *  The random calculated value for a counter is between 0 and the maximum, which is 
			 *  Limit * Seconds Diff
			 */
			
			
			//mocking RandomStrategy.getTimestamp()
			long actualTs=System.currentTimeMillis();
			RandomStrategy mockedStrategy = Mockito.mock(RandomStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
			item.setStrategy(mockedStrategy);
			item.setValue("0");
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("10000000");//Limit
        
			// Travel 5 minutes to the pass
			item.setTsLastQuery(actualTs-300000);
			long diffInSeconds=300; //5 minutes
			long max=Long.parseLong(item.getValue())+ (diffInSeconds*Long.parseLong(item.getAux()));
					
			//When
			long calculatedValue= Long.parseLong(item.calculate());
			
			//Then
			Assertions.assertTrue(calculatedValue <= max && calculatedValue>=0);
		}
    }
    
    @Nested
    @DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
    class Item_Counter_General_tests {
    	    	
		@DisplayName("Item counter completes one round and reset itself adding its leftover -1 with UsageAtPercent Strategy")
        @ParameterizedTest(name = "With an \"{0}\" counter increase and actual value equals 1000 to Long MAX_VALUE")
        @ValueSource(strings = { "1000", "1050", "2500" })		
		void Item_counter_completes_one_round_and_reset_itself_with_remainder_usageatpercent(String increase) throws WrongItemTypeForSelectedStrategyException {
			//Given
			
			long actualTs=System.currentTimeMillis();
	        UsageAtPercentStrategy mockedStrategy = Mockito.mock(UsageAtPercentStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
	        long limit=Long.parseLong(increase);
			item.setStrategy(mockedStrategy);
			item.setValue(Long.toString(Long.MAX_VALUE));
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("{ \"limit\": \""+ limit +"\", \"percentage\": \""+100+"\"}");
			// Travel 1 second to the pass
			item.setTsLastQuery(actualTs-1000);
			long diffInSeconds=1; //1 sec
			long expectedValue=(long)(diffInSeconds*limit)-1;
			Mockito.when(mockedStrategy.getMax(item, diffInSeconds*limit)).thenCallRealMethod();
			//When
			String calculated= item.calculate();

			//Then
			Assertions.assertEquals(expectedValue,Long.parseLong(calculated));			
		}
		
		@Test
		void Item_counter_completes_one_round_and_reset_itself_adding_its_leftover_minus_one_with_RandomStrategy() throws WrongItemTypeForSelectedStrategyException {
			//Given
			
			//mocking RandomStrategy.getTimestamp()
			long actualTs=System.currentTimeMillis();
			RandomStrategy mockedStrategy = Mockito.mock(RandomStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
			item.setStrategy(mockedStrategy);
			item.setValue(Long.toString(Long.MAX_VALUE));
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("10000000");//Limit
        
			// Travel 1 minutes to the pass
			item.setTsLastQuery(actualTs-1000);
			long diffInSeconds=1; //1 sec
			long max=diffInSeconds*Long.parseLong(item.getAux());
			Mockito.when(mockedStrategy.getMax(item, (diffInSeconds*Long.parseLong(item.getAux())))).thenCallRealMethod();
			//When
			long calculatedValue= Long.parseLong(item.calculate());
			
			//Then
			Assertions.assertTrue(calculatedValue < max && calculatedValue>=0);		
		}

		
		@Test
		void Item_counter_completes_one_round_and_reset_itself_adding_its_leftover_minus_one_with_ExhaustedStrategy() throws WrongItemTypeForSelectedStrategyException {
			//mocking ExhaustedStrategy.getTimestamp()
			long actualTs=System.currentTimeMillis();
	        ExhaustedStrategy mockedStrategy = Mockito.mock(ExhaustedStrategy.class);
	        Mockito.when(mockedStrategy.getTimeStamp()).thenReturn(actualTs);
	        Mockito.when(mockedStrategy.getValue(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.calculate(item)).thenCallRealMethod();
	        Mockito.when(mockedStrategy.getType()).thenReturn(ItemType.COUNTER);
	        
			item.setStrategy(mockedStrategy);
			item.setValue(Long.toString(Long.MAX_VALUE));
			item.setMin("0");
			item.setMax(Long.toString(Long.MAX_VALUE));
			item.setAux("10000000");//Limit
        
			// Travel 1 second to the pass
			item.setTsLastQuery(actualTs-1000);
			long diffInSeconds=1; //1 sec
			long max=diffInSeconds*Long.parseLong(item.getAux());
	        Mockito.when(mockedStrategy.getMax(item, (diffInSeconds*Long.parseLong(item.getAux())))).thenCallRealMethod();
			long low=(long)(max*0.98);				
			
			//When
			String calculated= item.calculate();

			//Then
			Assertions.assertTrue(Long.parseLong(calculated)>=low &&
									Long.parseLong(calculated)<=max
					);			
		}
		
		
		
		@Test
		void Item_with_different_type_than_assigned_strategy_type_causes_exception() throws WrongItemTypeForSelectedStrategyException {
			//Given
			item.setType(ItemType.GAUGE);
			item.setStrategy(new ZeroStrategy());
			item.setValue("old value");
			item.setPreviousValue("previous value");
			
			
			//When
			Exception exception = Assertions.assertThrows(WrongItemTypeForSelectedStrategyException.class, 
									() -> {
										item.calculate();
								    });
			
			
			//Then
				
			Assertions.assertEquals("The item has a wrong type for the specified generate values strategy. Item type must be '"+ItemType.COUNTER.toString()+"'"
					, exception.getMessage());
			


		}		
    }
}


