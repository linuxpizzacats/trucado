package org.lpc.resources;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.lpc.models.Item;
import org.lpc.models.dto.ItemDTO;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.repositories.ItemRepository;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.Header;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;
import static org.hamcrest.CoreMatchers.is;

import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

@QuarkusTest
@DisplayName("Testing Item Resource")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemResourceTest {
    
	@Inject
	ItemRepository repository;
	
	@Test
    void Test_get_item() {
		int id=1;
        given()
        .pathParam("id", id)
        .when().get("/item/{id}")
        .then()
          .statusCode(200)
        .body("oid", is(".1.3.6.1.2.1.1.1.0"));     
    }

	@Test
    void Test_get_non_existing_item() {
		
        given()
        .pathParam("id", 9999999)
        .when().get("/item/{id}")
        .then()
          .statusCode(204);     
    }
	
	@Test
    void Test_create_item() {
		ItemDTO itemDTO= new ItemDTO();
		
		itemDTO.setOid("9.9.9.9.9.9.9.9.9");
		itemDTO.setValue("0");
		itemDTO.setPreviousValue("0");
		itemDTO.setTsLastQuery(System.currentTimeMillis());
		itemDTO.setMax("100");
		itemDTO.setMin("0");
		itemDTO.setAux("");
		itemDTO.setStrategyName("ZeroStrategy");
		itemDTO.setType("STRING");
		
		Jsonb jsonb = JsonbBuilder.create();
		String itemDTOInJson = jsonb.toJson(itemDTO);
		with().body(itemDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().post("/item")
        .then()
          .statusCode(201)
        .body("oid", is(itemDTO.getOid()));
		
		try {
			repository.delete(repository.findByOid("9.9.9.9.9.9.9.9.9"));
		} catch (ItemNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    }
	@Test
    void Test_try_to_create_a_protected_item() {
		ItemDTO itemDTO= new ItemDTO();
		
		itemDTO.setOid(".1.3.6.1.2.1.1.6.0");
		itemDTO.setValue("0");
		itemDTO.setPreviousValue("0");
		itemDTO.setTsLastQuery(System.currentTimeMillis());
		itemDTO.setMax("100");
		itemDTO.setMin("0");
		itemDTO.setAux("");
		itemDTO.setStrategyName("ZeroStrategy");
		itemDTO.setType("STRING");
		
		Jsonb jsonb = JsonbBuilder.create();
		String itemDTOInJson = jsonb.toJson(itemDTO);
		with().body(itemDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().post("/item")
        .then()
          .statusCode(403);

    }	
	
	@Test
    void Test_delete_item() {
		Item item=new Item();
		item.setOid("9.9.9.9.9.9.9.9.9");
		item=repository.create(item);

		
		
        given()
        .pathParam("id", item.getId())
        .when().delete("/item/{id}")
        .then()
          .statusCode(200)
        .body(is("Item deleted")); 
		
    }
	
	@Test
    void Test_delete_non_existing_item() {	
        given()
        .pathParam("id", 999999999)
        .when().delete("/item/{id}")
        .then()
          .statusCode(204); 
		
    }
	
	@Test
    void Test_update_item() throws ItemNotFoundException {
		Item item=repository.findById(1);
		String originalOid=item.getOid();// for reestablish
		ItemDTO itemDTO= new ItemDTO(item);
		itemDTO.setOid("9.9.9.9.9.9.9.9.9");
		
		Jsonb jsonb = JsonbBuilder.create();
		String itemDTOInJson = jsonb.toJson(itemDTO);
		with().body(itemDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().put("/item")
        .then()
          .statusCode(200)
        .body("oid", is(itemDTO.getOid()));
		
		itemDTO.setOid(originalOid);
		itemDTOInJson = jsonb.toJson(itemDTO);
		with().body(itemDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().put("/item")
        .then()
          .statusCode(200)
        .body("oid", is(itemDTO.getOid()));
				
		
    }	
}
