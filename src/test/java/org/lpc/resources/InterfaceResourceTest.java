package org.lpc.resources;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.lpc.models.dto.ICommandDTO;
import org.lpc.models.dto.InterfaceCreateMinimalDTO;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.Header;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;
import static org.hamcrest.CoreMatchers.is;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

@QuarkusTest
@DisplayName("Testing Item Resource")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class InterfaceResourceTest {
    
	
	@Test
    void Test_get_interface() {
		int id=1;
        given()
        .pathParam("id", id)
        .when().get("/interface/{id}")
        .then()
          .statusCode(200)
        .body("id", is(id));     
    }

	@Test
    void Test_change_behaviour_interface() {
		int id=1;
		ICommandDTO commandDTO=new ICommandDTO();
		commandDTO.setBehaviour("org.lpc.models.commands.interfaces.AdminDown");
		Jsonb jsonb = JsonbBuilder.create();
		String commandDTOInJson = jsonb.toJson(commandDTO);
		with().pathParam("id", id).body(commandDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().patch("/interface/{id}")
        .then()
          .statusCode(204);
    }
	@Test
    void Test_change_behaviour_interface_to_Usage_At() {
		int id=1;
		ICommandDTO commandDTO=new ICommandDTO();
		commandDTO.setBehaviour("org.lpc.models.commands.interfaces.UsageAt");
		Jsonb jsonb = JsonbBuilder.create();
		String commandDTOInJson = jsonb.toJson(commandDTO);
		with().pathParam("id", id).body(commandDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().patch("/interface/{id}")
        .then()
          .statusCode(204);
    }	
	@Test
    void Test_change_behaviour_interface_to_Usage_In_At() {
		int id=1;
		ICommandDTO commandDTO=new ICommandDTO();
		commandDTO.setBehaviour("org.lpc.models.commands.interfaces.UsageInAt");
		commandDTO.setAux("50");
		Jsonb jsonb = JsonbBuilder.create();
		String commandDTOInJson = jsonb.toJson(commandDTO);
		with().pathParam("id", id).body(commandDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().patch("/interface/{id}")
        .then()
          .statusCode(204);
    }		
	@Test
    void Test_change_behaviour_interface_to_Usage_Out_At() {
		int id=1;
		ICommandDTO commandDTO=new ICommandDTO();
		commandDTO.setBehaviour("org.lpc.models.commands.interfaces.UsageOutAt");
		commandDTO.setAux("50");
		Jsonb jsonb = JsonbBuilder.create();
		String commandDTOInJson = jsonb.toJson(commandDTO);
		with().pathParam("id", id).body(commandDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().patch("/interface/{id}")
        .then()
          .statusCode(204);
    }	
	@Test
    void Test_create_interface() {
		InterfaceCreateMinimalDTO interfaceCreateMinimalDTO= new InterfaceCreateMinimalDTO();
		interfaceCreateMinimalDTO.setName("GigaEthernet2");
		interfaceCreateMinimalDTO.setHighspeed("1000");
		Jsonb jsonb = JsonbBuilder.create();
		String interfaceCreateMinimaDTOInJson = jsonb.toJson(interfaceCreateMinimalDTO);
		with().body(interfaceCreateMinimaDTOInJson).header(new Header("Content-Type", "application/json"))
        .when().post("/interface")
        .then()
          .statusCode(201);
    }
	@Test
    void Test_list_interfaces() {
		
		given()
        .when().get("/interface")
        .then()
          .statusCode(200);
    }		
}
