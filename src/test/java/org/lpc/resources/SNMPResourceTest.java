package org.lpc.resources;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsStringIgnoringCase;

@QuarkusTest
@DisplayName("Testing SNMP Resource")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class SNMPResourceTest {
    
	@Test
    void Test_get_endpoint() {
		String oid=".1.3.6.1.2.1.1.1.0"; //sysDesc
        given()
        .pathParam("oid", oid)
        .when().get("/get/{oid}")
        .then()
          .statusCode(200)
          .body(containsStringIgnoringCase("Fake SNMP device with trucado"));
    }
	
	@Test
    void Test_next_endpoint() {
		String oid=".1.3.6.1.2.1.1.1.0"; //sysDesc
        given()
        .pathParam("oid", oid)
        .when().get("/next/{oid}")
        .then()
          .statusCode(200)
          .body(containsStringIgnoringCase(".1.3.6.1.2.1.1.3.0"));
    }	

}
