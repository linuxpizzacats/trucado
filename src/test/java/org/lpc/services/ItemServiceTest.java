package org.lpc.services;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.services.exceptions.EndOfMibTreeReachedException;
import org.lpc.services.exceptions.ItemTypeBuilderNotFoundException;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@DisplayName("Testing Item Service")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class ItemServiceTest {

	@Inject
	ItemService service;
	
	@DisplayName("Create a new Item passing its desired type")
    @ParameterizedTest(name = "Create a type \"{0}\" item.")
    @ValueSource(strings = {"GAUGE","STRING", "COUNTER"})
	void Create_a_new_Item_passing_its_type(String type) throws ItemTypeBuilderNotFoundException {
		//Given
		Item newItem=service.create(type);
				
		//When
		ItemType itemType=ItemType.valueOf(type);
		
		//Then
		Assertions.assertEquals(itemType, newItem.getType());
	}
	
	@Test
	void Create_a_new_Item_with_not_valid_type_throws_ItemTypeBuilderNotFoundException() {
		//Given
		Exception exception=Assertions.assertThrows(ItemTypeBuilderNotFoundException.class, 
							() -> {
								service.create("NON-EXISTING-TYPE");
						    }); 
				
				
				
				
		//When
		Exception exceptionEnum=Assertions.assertThrows(IllegalArgumentException.class, 
								() -> {
									ItemType.valueOf("NON-EXISTING-TYPE");
							    }); 
				
				
		
		//Then IllegalArgumentException
		Assertions.assertEquals("The Item type 'NON-EXISTING-TYPE' does not exist.", exception.getMessage());
		Assertions.assertEquals("IllegalArgumentException",exceptionEnum.getClass().getSimpleName());
		
	}
	
	@Test
	void Get_an_item() throws ItemNotFoundException {
		//Given
		String oid=".1.3.6.1.2.1.1.1.0";
				
		//When
		Item item=service.get(oid);
		
		//Then
		Assertions.assertEquals(oid, item.getOid());
	}
	
	@Test
	void Get_a_non_existing_item() throws ItemNotFoundException {
		//Given
		String noExistingOid=".1.3.6.1.2.1.1.1.999999";
				
		//When
		Exception exception = Assertions.assertThrows(ItemNotFoundException.class, 
				() -> {
					service.get(noExistingOid);
			    });


		//Then
		
		Assertions.assertEquals("The item with oid '"+noExistingOid+"' does not exist.", exception.getMessage());
	}	
	
	@Test
	void Get_next_item() throws ItemNotFoundException, EndOfMibTreeReachedException {
		//Given
		String oid=".1.3.6.1.2.1.1.1.0";
		String nextOid=".1.3.6.1.2.1.1.3.0";
		
		//When
		Item item=service.next(oid);


		//Then
		
		Assertions.assertEquals(nextOid, item.getOid());
	}
	@Test
	void Try_to_get_next_item_and_end_of_mib_tree_reached_instead() throws ItemNotFoundException, EndOfMibTreeReachedException {
		//Given
		String oid=".1.3.6.1.2.1.31.1.1.1.6.1";

		
		//When
		Exception exception = Assertions.assertThrows(EndOfMibTreeReachedException.class, 
				() -> {
					service.next(oid);
			    });



		//Then
		
		Assertions.assertEquals("The end of the mib tree was reached.", exception.getMessage());
	}	
	
}
