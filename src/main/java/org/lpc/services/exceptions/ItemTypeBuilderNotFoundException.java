package org.lpc.services.exceptions;


public class ItemTypeBuilderNotFoundException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = 8898238078542138294L;

	public ItemTypeBuilderNotFoundException(String type) {
		super("The Item type '"+type+"' does not exist.");
	}
}
