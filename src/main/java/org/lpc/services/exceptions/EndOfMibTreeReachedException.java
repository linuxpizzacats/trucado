package org.lpc.services.exceptions;

public class EndOfMibTreeReachedException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8741519928674007480L;
	public EndOfMibTreeReachedException() {
		super("The end of the mib tree was reached.");
	}
}
