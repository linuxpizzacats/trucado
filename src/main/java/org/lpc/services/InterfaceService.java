package org.lpc.services;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.lpc.models.Interface;
import org.lpc.models.commands.interfaces.ICommand;
import org.lpc.models.dto.ICommandDTO;
import org.lpc.models.dto.InterfaceCreateMinimalDTO;
import org.lpc.models.dto.InterfaceDTO;
import org.lpc.models.exceptions.BehaviourNotFoundException;
import org.lpc.models.exceptions.InterfaceNotFoundException;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.repositories.InterfaceRepository;

@ApplicationScoped
public class InterfaceService {
	

	
	@Inject
	InterfaceRepository repository;
	
	public List<InterfaceDTO> get(){
		List<InterfaceDTO> listToReturn=new ArrayList<>();
		for (Interface interfaz : repository.findAll().list()) {
			listToReturn.add(new InterfaceDTO(interfaz));
		}
		return listToReturn;
	}
	
	public Interface get(long id)throws InterfaceNotFoundException {
		return repository.findById(id);
	}
	
	public InterfaceDTO getDTO(long id) throws InterfaceNotFoundException {
		return new InterfaceDTO(repository.findById(id));
	}
	
	public void setBehaviour(Interface interfaz,ICommandDTO behaviourDTO) throws BehaviourNotFoundException {
		try {
			ICommand behaviour=(ICommand) Class.forName(behaviourDTO.getBehaviour()).getDeclaredConstructor().newInstance();
			interfaz=this.getSpecialSets(interfaz, behaviourDTO);//for complex aux json fields
			interfaz.apply(behaviour);
			repository.update(interfaz);
		}catch (Exception exception) {
			throw new BehaviourNotFoundException();
		}
	}
	
	private Interface getSpecialSets(Interface interfaz,ICommandDTO behaviourDTO) {
		Integer limit=(Integer.parseInt(interfaz.getHighspeed().getValue()) * 1000 * 1000)/8;
		if(behaviourDTO.getBehaviour().equals("org.lpc.models.commands.interfaces.UsageAt")) {
			interfaz.getHcInOctects().setAux("{ \"limit\": \""+limit.toString()+"\", \"percentage\": \""+behaviourDTO.getAux()+"\"}");
			interfaz.getHcOutOctects().setAux("{ \"limit\": \""+limit.toString()+"\", \"percentage\": \""+behaviourDTO.getAux()+"\"}");
		}else if(behaviourDTO.getBehaviour().equals("org.lpc.models.commands.interfaces.UsageInAt")) {
			interfaz.getHcInOctects().setAux("{ \"limit\": \""+limit.toString()+"\", \"percentage\": \""+behaviourDTO.getAux()+"\"}");			
		}else if(behaviourDTO.getBehaviour().equals("org.lpc.models.commands.interfaces.UsageOutAt")) {
			interfaz.getHcOutOctects().setAux("{ \"limit\": \""+limit.toString()+"\", \"percentage\": \""+behaviourDTO.getAux()+"\"}");				
		}
		return interfaz;
	}
	
	public Interface create(InterfaceCreateMinimalDTO interfaceCreateMinimalDTO) throws WrongItemTypeForSelectedStrategyException {
		var interfaz=new Interface();
		interfaz.getName().setAux(interfaceCreateMinimalDTO.getName());
		interfaz.getName().setStrategyName("FixedAtStrategy");
		interfaz.getName().setStrategy(new org.lpc.models.strategies.items.string.FixedAtStrategy());
		interfaz.getName().calculate();
		interfaz.getHighspeed().setAux(interfaceCreateMinimalDTO.getHighspeed());
		interfaz.getHighspeed().setStrategyName("FixedAtStrategy");
		interfaz.getHighspeed().setStrategy(new org.lpc.models.strategies.items.gauge.FixedAtStrategy());
		interfaz.getHighspeed().calculate();
		interfaz.getOperStatus().setStrategyName("FixedAtStrategy");
		interfaz.getOperStatus().setStrategy(new org.lpc.models.strategies.items.gauge.FixedAtStrategy());
		interfaz.getOperStatus().setAux("2");
		interfaz.getOperStatus().calculate();
		interfaz.getAdminStatus().setStrategyName("FixedAtStrategy");
		interfaz.getAdminStatus().setStrategy(new org.lpc.models.strategies.items.gauge.FixedAtStrategy());
		interfaz.getAdminStatus().setAux("2");
		interfaz.getAdminStatus().calculate();
		interfaz.getAlias().setStrategyName("FixedAtStrategy");
		interfaz.getAlias().setStrategy(new org.lpc.models.strategies.items.string.FixedAtStrategy());
		interfaz.getAlias().setAux("");
		interfaz.getAlias().calculate();
		interfaz.getIndex().setStrategyName("FixedAtStrategy");
		interfaz.getIndex().setStrategy(new org.lpc.models.strategies.items.string.FixedAtStrategy());		
		long quantity=repository.count();
		interfaz.getIndex().setAux(Long.toString(quantity+1));
		interfaz.getIndex().calculate();
		return repository.create(interfaz);
	}
}
