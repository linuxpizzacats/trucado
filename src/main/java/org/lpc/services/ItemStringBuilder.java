package org.lpc.services;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;


public class ItemStringBuilder extends ItemBuilder{

	@Override
	public ItemBuilder newItem() {
		this.item=new Item(ItemType.STRING);
		return this;
	}

}
