package org.lpc.services;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;
import org.lpc.services.exceptions.ItemTypeBuilderNotFoundException;

@Singleton
public class ItemFactory {
	
	private Map<String,ItemBuilder> builders;
	private Map<ItemType,Map<String,Strategy>> strategies;
	
	public ItemFactory() {
		final var EXHAUSTED_STRATEGY="ExhaustedStrategy";
		final var FIXED_AT_MAX_STRATEGY="FixedAtMaxStrategy";
		final var FIXED_AT_MIN_STRATEGY="FixedAtMinStrategy";
		final var FIXED_AT_STRATEGY="FixedAtStrategy";
		final var FLAPPING_STRATEGY="FlappingStrategy";
		final var RANDOM_STRATEGY="RandomStrategy";
		final var USAGE_AT_PERCENT_STRATEGY="UsageAtPercentStrategy";
		final var ZERO_STRATEGY="ZeroStrategy";
		
		this.setBuilders(new HashMap<>());
		this.setStrategies(new HashMap<>());
		this.getBuilders().put("STRING", new ItemStringBuilder());
		this.getBuilders().put("GAUGE", new ItemGaugeBuilder());
		this.getBuilders().put("COUNTER", new ItemCounterBuilder());
		strategies.put(ItemType.GAUGE,Map.ofEntries(
				  new HashMap.SimpleEntry<String, Strategy>(EXHAUSTED_STRATEGY, new org.lpc.models.strategies.items.gauge.ExhaustedStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MAX_STRATEGY, new org.lpc.models.strategies.items.gauge.FixedAtMaxStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MIN_STRATEGY, new org.lpc.models.strategies.items.gauge.FixedAtMinStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_STRATEGY, new org.lpc.models.strategies.items.gauge.FixedAtStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FLAPPING_STRATEGY, new org.lpc.models.strategies.items.gauge.FlappingStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(RANDOM_STRATEGY, new org.lpc.models.strategies.items.gauge.RandomStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(USAGE_AT_PERCENT_STRATEGY, new org.lpc.models.strategies.items.gauge.UsageAtPercentStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(ZERO_STRATEGY, new org.lpc.models.strategies.items.gauge.ZeroStrategy())
				));
		strategies.put(ItemType.COUNTER,Map.ofEntries(
				  new HashMap.SimpleEntry<String, Strategy>(EXHAUSTED_STRATEGY, new org.lpc.models.strategies.items.counter.ExhaustedStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MAX_STRATEGY, new org.lpc.models.strategies.items.counter.FixedAtMaxStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MIN_STRATEGY, new org.lpc.models.strategies.items.counter.FixedAtMinStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_STRATEGY, new org.lpc.models.strategies.items.counter.FixedAtStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(FLAPPING_STRATEGY, new org.lpc.models.strategies.items.counter.FlappingStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(RANDOM_STRATEGY, new org.lpc.models.strategies.items.counter.RandomStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(USAGE_AT_PERCENT_STRATEGY, new org.lpc.models.strategies.items.counter.UsageAtPercentStrategy()),
				  new HashMap.SimpleEntry<String, Strategy>(ZERO_STRATEGY, new org.lpc.models.strategies.items.counter.ZeroStrategy())
				));
		strategies.put(ItemType.STRING,Map.ofEntries(
			    new HashMap.SimpleEntry<String, Strategy>(EXHAUSTED_STRATEGY, new org.lpc.models.strategies.items.string.ExhaustedStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MAX_STRATEGY, new org.lpc.models.strategies.items.string.FixedAtMaxStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_MIN_STRATEGY, new org.lpc.models.strategies.items.string.FixedAtMinStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(FIXED_AT_STRATEGY, new org.lpc.models.strategies.items.string.FixedAtStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(FLAPPING_STRATEGY, new org.lpc.models.strategies.items.string.FlappingStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(RANDOM_STRATEGY, new org.lpc.models.strategies.items.string.RandomStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(USAGE_AT_PERCENT_STRATEGY, new org.lpc.models.strategies.items.string.UsageAtPercentStrategy()),
			    new HashMap.SimpleEntry<String, Strategy>(ZERO_STRATEGY, new org.lpc.models.strategies.items.string.ZeroStrategy())
			));		
		
	}
	
	public ItemBuilder getBuilder(String type) throws ItemTypeBuilderNotFoundException {
		if(this.getBuilders().containsKey(type)) {
			return this.getBuilders().get(type);
		}else {
			throw new ItemTypeBuilderNotFoundException(type);
		}
	}

	public Map<ItemType, Map<String,Strategy>> getStrategies() {
		return strategies;
	}

	public void setStrategies(Map<ItemType, Map<String,Strategy>> strategies) {
		this.strategies = strategies;
	}

	public Map<String, ItemBuilder> getBuilders() {
		return builders;
	}

	public void setBuilders(Map<String, ItemBuilder> builders) {
		this.builders = builders;
	}
	
	public Strategy getStrategy(Item item) {
		return this.getStrategies().get(item.getType()).get(item.getStrategyName());
	}
	public ItemType getType(String type) {
		return ItemType.valueOf(ItemType.class, type);
	}
	public Item setItemStrategy(Item item) {
		var stg=this.getStrategy(item);
		item.setStrategy(stg);
		return item;
	}
}
