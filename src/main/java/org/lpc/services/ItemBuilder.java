package org.lpc.services;

import org.lpc.models.Item;


public abstract class ItemBuilder {
	protected Item item;
	public abstract ItemBuilder newItem();
	public Item getResult() {
		return this.item;
	}
}
