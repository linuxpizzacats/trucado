package org.lpc.services;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.lpc.models.Item;
import org.lpc.models.dto.ItemDTO;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.models.exceptions.ProtectedOidCreationException;
import org.lpc.repositories.ItemRepository;
import org.lpc.services.exceptions.EndOfMibTreeReachedException;
import org.lpc.services.exceptions.ItemTypeBuilderNotFoundException;

@ApplicationScoped
public class ItemService {
	
	List<String> protectedOids=Arrays.asList(new String[]{".1.3.6.1.2.1.1.1.0", ".1.3.6.1.2.1.1.3.0", ".1.3.6.1.2.1.1.4.0",
															".1.3.6.1.2.1.1.5.0", ".1.3.6.1.2.1.1.6.0"}); // system protected oids
	
	@Inject
	ItemRepository repository;
	
	public Item create(String type) throws ItemTypeBuilderNotFoundException {
		return repository.getNew(type);
	}
	public Item create(ItemDTO itemDTO) throws ItemTypeBuilderNotFoundException, ProtectedOidCreationException {
		if(!protectedOids.contains(itemDTO.getOid())){
			return repository.create(itemDTO);
		}else {
			throw new ProtectedOidCreationException();
		}
	}	
	public Item get(String oid) throws ItemNotFoundException {
		return repository.findByOid(oid);
	}
	public Item get(long id) throws ItemNotFoundException {
		return repository.findById(id);
	}	
	
	public Item update(ItemDTO itemDTO) throws ItemNotFoundException {
		return repository.update(itemDTO);
	}
	
	public boolean delete(long id) {
		return repository.delete(id);
	}
	
	public ItemDTO getDTO(long id) throws ItemNotFoundException {
		return new ItemDTO(this.get(id));
	}
	public Item next(String oid) throws EndOfMibTreeReachedException, ItemNotFoundException {
		var item=repository.findByOidLike(oid);
		if(item.getOid().equals(oid)){// I found the same that they are asking the next for.
			var next=false;
			for (Item itemIteration : repository.findAllItems()) {
				if(next) {
					item=itemIteration;
					break;
				}
				if(itemIteration.equals(item)) {
					next=true;
				}
			}
			if(item.getOid().equals(oid)) {
				throw new EndOfMibTreeReachedException();
			}
		}
		
		return item;
	}	
	
	public String getSNMPResponse(Item item) throws ItemNotFoundException {
		String snmpStr=item.toString();
		repository.update(item);
		return snmpStr;
	}
}
