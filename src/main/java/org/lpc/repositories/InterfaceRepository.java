package org.lpc.repositories;

import java.lang.reflect.InvocationTargetException;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.jboss.logging.Logger;
import org.lpc.models.Interface;
import org.lpc.models.Item;
import org.lpc.models.exceptions.InterfaceNotFoundException;
import org.lpc.models.exceptions.ItemNotFoundException;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class InterfaceRepository implements PanacheRepository<Interface> {
	
	@Inject
	EntityManager em;
	
	@Inject
	ItemRepository itemRepository;
	
	@Transactional
	public Interface findById(long id) throws InterfaceNotFoundException {
		Interface interfaz=em.find(Interface.class, id);
		if(interfaz==null) {
			throw new InterfaceNotFoundException(id);
		}
		return interfaz;
	}

	@Transactional
	public Interface create(Interface interfaz) {
		em.persist(interfaz);
		return interfaz;
		
	}

	@Transactional
	public Interface update(Interface interfaz) throws InterfaceNotFoundException, ItemNotFoundException {
		var updatedInterface=this.findById(interfaz.getId());
		
		this.verifyChanges(updatedInterface, updatedInterface.getAdminStatus(), interfaz.getAdminStatus(),"AdminStatus");
		this.verifyChanges(updatedInterface, updatedInterface.getAlias(), interfaz.getAlias(),"Alias");
		this.verifyChanges(updatedInterface, updatedInterface.getHcInOctects(), interfaz.getHcInOctects(),"HcInOctects");
		this.verifyChanges(updatedInterface, updatedInterface.getHcOutOctects(), interfaz.getHcOutOctects(),"HcOutOctects");
		this.verifyChanges(updatedInterface, updatedInterface.getHighspeed(), interfaz.getHighspeed(),"Highspeed");
		this.verifyChanges(updatedInterface, updatedInterface.getIndex(), interfaz.getIndex(),"Index");
		this.verifyChanges(updatedInterface, updatedInterface.getInDiscards(), interfaz.getInDiscards(),"InDiscards");
		this.verifyChanges(updatedInterface, updatedInterface.getInErrors(), interfaz.getInErrors(),"InErrors");
		this.verifyChanges(updatedInterface, updatedInterface.getName(), interfaz.getName(),"Name");
		this.verifyChanges(updatedInterface, updatedInterface.getOperStatus(), interfaz.getOperStatus(),"OperStatus");
		this.verifyChanges(updatedInterface, updatedInterface.getOutDiscards(), interfaz.getOutDiscards(),"OutDiscards");
		this.verifyChanges(updatedInterface, updatedInterface.getOutErrors(), interfaz.getOutErrors(),"OutErrors");
		
		
		return updatedInterface;		
	}
	
	@Transactional
	public void delete(Interface interfaz) {
		delete("ID",interfaz.getId());
	}
	
	
	private Interface verifyChanges(Interface interfaz,Item itemOriginal, Item itemPosiblyNew,String itemName) throws ItemNotFoundException {
		itemRepository.findById(itemOriginal.getId());
		if(itemPosiblyNew.getId()==0) {
			
			try {
				var setMethod = interfaz.getClass().getMethod("set"+itemName, Item.class);
				setMethod.invoke(interfaz, itemPosiblyNew);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				var log=Logger.getLogger(InterfaceRepository.class);
				log.error(e.getMessage());
			}
		}else if(!itemOriginal.equals(itemPosiblyNew)){
			itemRepository.update(itemPosiblyNew);
		}	
		return interfaz;
	}
}
