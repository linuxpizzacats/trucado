package org.lpc.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.lpc.models.Item;
import org.lpc.models.dto.ItemDTO;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.services.ItemFactory;
import org.lpc.services.exceptions.ItemTypeBuilderNotFoundException;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class ItemRepository implements PanacheRepository<Item> {
	
	@Inject
	EntityManager em;

	@Inject
	ItemFactory factory;
	
	@Transactional
	public Item findById(long id) throws ItemNotFoundException {
		var item=em.find(Item.class, id);
		if(item==null) {
			throw new ItemNotFoundException(id);
		}
		return factory.setItemStrategy(item); 
	}
	
	@Transactional
	public List<Item> findAllItems() {
		List<Item> list= new ArrayList<>();
		for (Item item : em.createQuery("FROM Item ORDER BY oid",Item.class).getResultList()) {
			list.add(factory.setItemStrategy(item)); 
		}
		return list;
	}
	
	@Transactional
	public Item findByOidLike(String oid) throws ItemNotFoundException {
		try {
			var item= em.createQuery("FROM Item WHERE oid LIKE :oid",Item.class).setParameter("oid", oid+"%").getSingleResult();
			return factory.setItemStrategy(item);
		}catch (Exception e) {
			throw new ItemNotFoundException(oid);
		}
	}
	@Transactional
	public Item findByOid(String oid) throws ItemNotFoundException {
		var item=find("OID", oid).firstResult();
		
		if(item==null) {
			throw new ItemNotFoundException(oid);
		}
		
		return factory.setItemStrategy(item); 
	}	
	@Transactional
	public Item create(Item item) {
		em.persist(item);
		return factory.setItemStrategy(item);
	}
	
	@Transactional
	public Item create(ItemDTO itemDTO) throws ItemTypeBuilderNotFoundException {
		var item= this.getNew(itemDTO.getType());
		item.setOid(itemDTO.getOid());
		item.setValue(itemDTO.getValue());
		item.setPreviousValue(itemDTO.getPreviousValue());
		item.setTsLastQuery(itemDTO.getTsLastQuery());
		item.setMax(itemDTO.getMax());
		item.setMin(itemDTO.getMin());
		item.setAux(itemDTO.getAux());
		item.setStrategyName(itemDTO.getStrategyName());
		
		em.persist(item);
		return factory.setItemStrategy(item);
	}	
	
	public Item getNew(String type) throws ItemTypeBuilderNotFoundException {
		return factory.getBuilder(type).newItem().getResult();
	}
	
	@Transactional
	public Item update(Item item) throws ItemNotFoundException {
		var updatedItem=this.findById(item.getId());
		updatedItem.setAux(item.getAux());
		updatedItem.setMax(item.getMax());
		updatedItem.setMin(item.getMin());
		updatedItem.setOid(item.getOid());
		updatedItem.setPreviousValue(item.getPreviousValue());
		updatedItem.setStrategy(item.getStrategy());
		updatedItem.setStrategyName(item.getStrategyName());
		updatedItem.setTsLastQuery(item.getTsLastQuery());
		updatedItem.setType(item.getType());
		updatedItem.setValue(item.getValue());
		
		return item;
	}
	
	@Transactional
	public Item update(ItemDTO itemDTO) throws ItemNotFoundException {
		var updatedItem=this.findById(itemDTO.getId());
		updatedItem.setAux(itemDTO.getAux());
		updatedItem.setMax(itemDTO.getMax());
		updatedItem.setMin(itemDTO.getMin());
		updatedItem.setOid(itemDTO.getOid());
		updatedItem.setPreviousValue(itemDTO.getPreviousValue());
		updatedItem.setStrategyName(itemDTO.getStrategyName());
		updatedItem.setTsLastQuery(itemDTO.getTsLastQuery());
		updatedItem.setType(factory.getType(itemDTO.getType()));
		updatedItem.setValue(itemDTO.getValue());
		
		return factory.setItemStrategy(updatedItem);
	}	
	@Transactional
	public void delete(Item item) {
		delete("ID",item.getId());	
	}
	
	@Transactional
	public boolean delete(long id) {
		return deleteById(id);	
	}
}
