package org.lpc.models.enums;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public enum ItemType {
	GAUGE,
	COUNTER,
	STRING;
	
}
