package org.lpc.models.enums;


public enum InterfaceType {
	ETHERNET(10,"Ethernet"),FAST_ETHERNET(100,"FastEthernet"),GIGA_ETHERNET(1000,"GigaEthernet"), 
	TENGIGA_ETHERNET(10000,"TenGigaEthernet"), HUNDREDGIGA_ETHERNET(100000,"HundredGigaEthernet");
	
	private final int highspeed;
	private final String name;
	InterfaceType(int value, String nameaux){
		highspeed=value;
		name=nameaux;
	}
	public int getHighspeed() {
		return highspeed;
	}
	@Override
	public String toString() {
		return name;
	}
}
