package org.lpc.models;



import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import org.lpc.models.strategies.items.Strategy;
import org.lpc.models.strategies.items.string.ZeroStrategy;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;


@Entity
public class Item {
	private long id;
	private String oid;
	private String value;
	private String previousValue;
	private long tsLastQuery;
	private String max;
	private String min;
	private ItemType type;
	private String strategyName;
	private Strategy strategy;
	private String aux;
	
	public Item() {
		this.setId(0);
		this.setOid("");
		this.setValue("0");
		this.setPreviousValue("0");
		this.setTsLastQuery(System.currentTimeMillis());
		this.setMax("100");
		this.setMin("0");
		this.setAux("");
		this.setStrategyName("ZeroStrategy");
		this.setType(ItemType.STRING);
		this.setStrategy(new ZeroStrategy());
	}
	
	public Item(Strategy strategy) {
		this();
		this.setStrategy(strategy);
	}
	
	public Item(ItemType type) {
		this();
		this.setType(type);
	}	
	
	public Item(long id, String oid, String value, String previousValue, int tsLastQuery, String max, String min,
			Strategy strategy, String aux) {
		super();
		this.setId(id);
		this.setOid(oid);
		this.setValue(value);
		this.setPreviousValue(previousValue);
		this.setTsLastQuery(tsLastQuery);
		this.setMax(max);
		this.setMin(min);
		this.setStrategy(strategy);
		this.setAux(aux);
	}
	
	
	/*
	 * Getters
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public String getOid() {
		return oid;
	}
	public String getValue() {
		return value;
	}	
	public String getMax() {
		return max;
	}
	public String getMin() {
		return min;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPreviousValue() {
		return previousValue;
	}
	public long getTsLastQuery() {
		return tsLastQuery;
	}
	@Transient
	public Strategy getStrategy() {
		return strategy;
	}	
	public String getAux() {
		return aux;
	}
	
	@Enumerated(EnumType.STRING)
	public ItemType getType() {
		return type;
	}

	public String getStrategyName() {
		return strategyName;
	}
	
	/*
	 * Setters
	 */
	


	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}

	public void setType(ItemType type) {
		this.type = type;
	}

	public void setAux(String aux) {
		this.aux = aux;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}

	public void setPreviousValue(String previousValue) {
		this.previousValue = previousValue;
	}
	public void setTsLastQuery(long tsLastQuery) {
		this.tsLastQuery = tsLastQuery;
	}
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
		this.setStrategyName(strategy.getClass().getSimpleName());
	}
	public void setValue(String value) {
		this.value = value;	
	}

	public void setMax(String max) {
		this.max = max;
	}

	public void setMin(String min) {
		this.min = min;
	}


	public String calculate() throws WrongItemTypeForSelectedStrategyException {
		String calculatedValue=this.getStrategy().getValue(this);
		this.setTsLastQuery(System.currentTimeMillis());
		this.setPreviousValue(this.getValue());
		this.setValue(calculatedValue);
		return calculatedValue;
	}
	
	@Override
	public int hashCode() {
		return Integer.parseInt(this.getOid().replace(".", ""));
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Item) {
			var item= (Item)obj;
			
			return this.getId()==item.getId()
					&& this.getOid().equals(item.getOid())
					&& this.getValue().equals(item.getValue())
					&& this.getPreviousValue().equals(item.getPreviousValue())
					&& this.getTsLastQuery()==item.getTsLastQuery()
					&& this.getMax().equals(item.getMax())
					&& this.getMin().equals(item.getMin())
					&& this.getAux().equals(item.getAux())
					&& this.getStrategy().equals(item.getStrategy())
					&& this.getStrategyName().equals(item.getStrategyName())
					&& this.getType()==item.getType();
		}else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		try {
			return this.getOid()+System.lineSeparator()+this.getType().toString()+System.lineSeparator()+this.calculate()+System.lineSeparator();
		} catch (WrongItemTypeForSelectedStrategyException e) {
			return e.getMessage();
		}
	}
	
}
