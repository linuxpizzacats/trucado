package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.FixedAtStrategy;

public class AdminDown implements ICommand {
	@Override
	public void execute(Interface interfaz) {
		interfaz.apply(new OperDown());
		interfaz.getAdminStatus().setStrategy(new FixedAtStrategy());
		interfaz.getAdminStatus().setAux("2");
		try {
			interfaz.getAdminStatus().calculate();
		} catch (WrongItemTypeForSelectedStrategyException e) {
			System.err.println(e.getMessage());
		}
	}
}
