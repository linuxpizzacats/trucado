package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;

public interface ICommand {
	void execute(Interface interfaz);
}
