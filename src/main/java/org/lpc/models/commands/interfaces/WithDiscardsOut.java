package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.RandomStrategy;

public class WithDiscardsOut implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		// To advance more quickly, the behaviours takes predefined values that seems logic for Networking
		
		interfaz.getOutDiscards().setMin("0");
		interfaz.getOutDiscards().setMax(Long.toString(Long.MAX_VALUE));
		interfaz.getOutDiscards().setAux("500"); //Max possible value
		interfaz.getOutDiscards().setStrategy(new RandomStrategy());
	}

}
