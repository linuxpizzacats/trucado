package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.FixedAtStrategy;

public class OperUp implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		interfaz.getOperStatus().setStrategy(new FixedAtStrategy());
		interfaz.getOperStatus().setAux("1");		
		try {
			interfaz.getOperStatus().calculate();
		} catch (WrongItemTypeForSelectedStrategyException e) {
			System.err.println(e.getMessage());
		}		
	}

}
