package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.ZeroStrategy;

public class UsageInNone implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		interfaz.getHcInOctects().setStrategy(new ZeroStrategy());
	}

}
