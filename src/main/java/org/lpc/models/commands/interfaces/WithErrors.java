package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;

public class WithErrors implements ICommand {
	@Override
	public void execute(Interface interfaz) {

		interfaz.apply(new WithErrorsOut());
		interfaz.apply(new WithErrorsIn());
	}
}
