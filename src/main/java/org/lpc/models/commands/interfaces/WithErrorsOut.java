package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.RandomStrategy;

public class WithErrorsOut implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		// To advance more quickly, the behaviours takes predefined values that seems logic for Networking

		interfaz.getOutErrors().setMin("0");
		interfaz.getOutErrors().setMax(Long.toString(Long.MAX_VALUE));
		interfaz.getOutErrors().setAux("500"); //Max possible value		
		interfaz.getOutErrors().setStrategy(new RandomStrategy());
	}

}
