package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.FlappingStrategy;

public class Flapping implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		interfaz.getOperStatus().setStrategy(new FlappingStrategy());
		interfaz.getOperStatus().setAux("1,2");
		try {
			interfaz.getOperStatus().calculate();
		} catch (WrongItemTypeForSelectedStrategyException e) {
			System.err.println(e.getMessage());
		}
	}

}
