package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;

public class WithErrorsAndDiscards implements ICommand {
	@Override
	public void execute(Interface interfaz) {
		interfaz.apply(new WithErrors());
		interfaz.apply(new WithDiscards());
	}
}
