package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.RandomStrategy;

public class WithDiscardsIn implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		// To advance more quickly, the behaviours takes predefined values that seems logic for Networking
		
		interfaz.getInDiscards().setMin("0");
		interfaz.getInDiscards().setMax(Long.toString(Long.MAX_VALUE));
		interfaz.getInDiscards().setAux("500"); //Max possible value		
		interfaz.getInDiscards().setStrategy(new RandomStrategy());
	}

}
