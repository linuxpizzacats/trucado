package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.RandomStrategy;

public class WithErrorsIn implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		// To advance more quickly, the behaviours takes predefined values that seems logic for Networking
		
		interfaz.getInErrors().setMin("0");
		interfaz.getInErrors().setMax(Long.toString(Long.MAX_VALUE));
		interfaz.getInErrors().setAux("500"); //Max possible value
		interfaz.getInErrors().setStrategy(new RandomStrategy());
	}

}
