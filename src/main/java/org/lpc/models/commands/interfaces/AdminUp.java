package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.models.strategies.items.gauge.FixedAtStrategy;

public class AdminUp implements ICommand {
	@Override
	public void execute(Interface interfaz) {
		interfaz.apply(new OperUp());
		interfaz.getAdminStatus().setStrategy(new FixedAtStrategy());
		interfaz.getAdminStatus().setAux("1");	
		try {
			interfaz.getAdminStatus().calculate();
		} catch (WrongItemTypeForSelectedStrategyException e) {
			System.err.println(e.getMessage());
		}		
	}
}
