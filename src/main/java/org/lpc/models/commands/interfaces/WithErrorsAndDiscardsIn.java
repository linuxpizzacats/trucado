package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;

public class WithErrorsAndDiscardsIn implements ICommand {
	@Override
	public void execute(Interface interfaz) {
		interfaz.apply(new WithErrorsIn());
		interfaz.apply(new WithDiscardsIn());
	}
}
