package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;

public class WithDiscards implements ICommand {
	@Override
	public void execute(Interface interfaz) {
		interfaz.apply(new WithDiscardsOut());
		interfaz.apply(new WithDiscardsIn());
	}
}
