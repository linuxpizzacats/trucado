package org.lpc.models.commands.interfaces;

import org.lpc.models.Interface;
import org.lpc.models.strategies.items.counter.UsageAtPercentStrategy;

public class UsageInAt implements ICommand{

	@Override
	public void execute(Interface interfaz) {
		interfaz.getHcInOctects().setStrategy(new UsageAtPercentStrategy());
	}

}
