package org.lpc.models.strategies.items.counter;


import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class RandomStrategy extends Strategy{
	public RandomStrategy() {
		super();
		this.setType(ItemType.COUNTER);
	}
	@Override
	public String calculate(Item item) {

		long now=this.getTimeStamp();
		long diffInMillis=now-item.getTsLastQuery();
		long diffInSeconds=diffInMillis/1000;	
		
		return Long.toString(this.getMax(item, diffInSeconds*Long.parseLong(item.getAux())));
	}
	public long getMax(Item item, Long valueToSum) {
		/*
		 * Counter items can have overflow, that's why we use this method for sum the values
		 */
		valueToSum=(long)(Math.random() * valueToSum);
		Long sum=Long.parseLong(item.getValue())+valueToSum;
		if(sum < 0) {
			Long overflow=Long.MAX_VALUE-(Long.MAX_VALUE-valueToSum);
			sum=overflow-1;
		}
		return sum;
	}	

}
