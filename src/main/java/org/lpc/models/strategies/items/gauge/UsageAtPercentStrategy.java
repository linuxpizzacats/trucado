package org.lpc.models.strategies.items.gauge;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class UsageAtPercentStrategy extends Strategy {
	public UsageAtPercentStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}
	@Override
	public String calculate(Item item) {
		var max=Integer.parseInt(item.getMax());	
	
		return String.valueOf((Integer.parseInt(item.getAux()) * max)/100);
	}
	
}
