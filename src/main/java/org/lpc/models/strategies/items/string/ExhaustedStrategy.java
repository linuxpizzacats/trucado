package org.lpc.models.strategies.items.string;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class ExhaustedStrategy extends Strategy {
	public ExhaustedStrategy() {
		super();
		this.setType(ItemType.STRING);
	}
	public String calculate(Item item) {
		return item.getValue();
	}
	
}
