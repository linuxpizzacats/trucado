package org.lpc.models.strategies.items.string;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class FixedAtMinStrategy extends Strategy{
	public FixedAtMinStrategy() {
		super();
		this.setType(ItemType.STRING);
	}
	@Override
	public String calculate(Item item) {
		return item.getMin();
	}

}
