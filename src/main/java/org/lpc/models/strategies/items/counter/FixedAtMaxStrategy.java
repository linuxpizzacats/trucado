package org.lpc.models.strategies.items.counter;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class FixedAtMaxStrategy extends Strategy {
	public FixedAtMaxStrategy() {
		super();
		this.setType(ItemType.COUNTER);
	}
	@Override
	public String calculate(Item item) {

		return item.getMax();
	}

}
