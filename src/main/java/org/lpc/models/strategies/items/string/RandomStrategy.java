package org.lpc.models.strategies.items.string;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class RandomStrategy extends Strategy{
	public RandomStrategy() {
		super();
		this.setType(ItemType.STRING);
	}
	@Override
	public String calculate(Item item) {
		var value="";
	    MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(String.valueOf(System.currentTimeMillis()).getBytes());
			md.digest();
		    byte[] digest = md.digest();
		    value=DatatypeConverter.printHexBinary(digest).toUpperCase();	
		} catch (NoSuchAlgorithmException e) {
			value=String.valueOf(System.currentTimeMillis());
		}
		return value;
	}
	

}
