package org.lpc.models.strategies.items.gauge;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class FlappingStrategy extends Strategy{
	public FlappingStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}
	@Override
	public String calculate(Item item) {
		List<String> values= Arrays.asList(item.getAux().split(","));
		String value=item.getValue();
		if(!values.isEmpty()) {
			value=values.get(new Random().nextInt(values.size()));
		}
		return value;
	}

}
