package org.lpc.models.strategies.items.string;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class FixedAtStrategy extends Strategy {
	public FixedAtStrategy() {
		super();
		this.setType(ItemType.STRING);
	}
	@Override
	public String calculate(Item item) {
		
		return item.getAux();
	}

}
