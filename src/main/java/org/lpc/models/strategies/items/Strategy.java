package org.lpc.models.strategies.items;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;

public abstract class Strategy {
	private ItemType type;
	public abstract String calculate(Item item);
	public String getValue(Item item) throws WrongItemTypeForSelectedStrategyException {
		this.verifyItemType(item);
		return this.calculate(item);
	}
	
	public ItemType getType() {
		return type;
	}
	public void setType(ItemType type) {
		this.type = type;
	}
	private void verifyItemType(Item item) throws WrongItemTypeForSelectedStrategyException {
		if(!item.getType().equals(this.getType())) {
			throw new WrongItemTypeForSelectedStrategyException(this.getType());
		}
	}
	
	public long getTimeStamp() {
		return System.currentTimeMillis();
	}
	
}
