package org.lpc.models.strategies.items.string;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class UsageAtPercentStrategy extends Strategy {
	public UsageAtPercentStrategy() {
		super();
		this.setType(ItemType.STRING);
	}	
	@Override
	public String calculate(Item item) {
		return item.getValue();
	}
}
