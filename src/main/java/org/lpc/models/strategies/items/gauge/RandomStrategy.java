package org.lpc.models.strategies.items.gauge;


import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class RandomStrategy extends Strategy{
	public RandomStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}
	@Override
	public String calculate(Item item) {

		var max=Integer.parseInt(item.getMax());
		var min=Integer.parseInt(item.getMin());
		
		return Integer.toString((int)(min + (Math.random() * (max-min))));
	}
	

}
