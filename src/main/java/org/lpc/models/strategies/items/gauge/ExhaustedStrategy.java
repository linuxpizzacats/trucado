package org.lpc.models.strategies.items.gauge;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class ExhaustedStrategy extends Strategy {
	public ExhaustedStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}
	@Override
	public String calculate(Item item) {
		var max=Integer.parseInt(item.getMax());
		var low=(int)(max * 0.98);
		return Integer.toString((int)(low + (Math.random() * (max-low))));
	}
	
}
