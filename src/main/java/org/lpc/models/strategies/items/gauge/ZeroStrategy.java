package org.lpc.models.strategies.items.gauge;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class ZeroStrategy extends Strategy{
	public ZeroStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}	
	
	@Override
	public String calculate(Item item) {
		return "0";
	}

}
