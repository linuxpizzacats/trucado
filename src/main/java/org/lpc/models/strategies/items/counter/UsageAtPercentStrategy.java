package org.lpc.models.strategies.items.counter;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

import com.google.gson.Gson;

public class UsageAtPercentStrategy extends Strategy {
	public UsageAtPercentStrategy() {
		super();
		this.setType(ItemType.COUNTER);
	}
	@Override
	public String calculate(Item item) {
		var auxParams=new AuxParametersForCounterItem(item.getAux());
		long now=this.getTimeStamp();
		long diffInMillis=now-item.getTsLastQuery();
		long diffInSeconds=diffInMillis/1000;
		long max=this.getMax(item, diffInSeconds*auxParams.getLimit());
		return Long.toString(((long)(auxParams.getPercentage())*max)/100);
	}
	public long getMax(Item item, Long valueToSum) {
		/*
		 * Counter items can have overflow, that's why we use this method for sum the values
		 */
		Long sum=Long.parseLong(item.getValue())+valueToSum;
		if(sum < 0) {
			Long overflow=Long.MAX_VALUE-(Long.MAX_VALUE-valueToSum);
			sum=overflow-1;
		}
		return sum;
	}	
	
	class AuxParametersForCounterItem {
		private int percentage; 
	    private long limit;
	    
	    public AuxParametersForCounterItem(String jsonString) {
	    	var gson = new Gson();
	    	AuxParametersForCounterItem auxParamsObj = gson.fromJson(jsonString,AuxParametersForCounterItem.class);
	    	this.setLimit(auxParamsObj.getLimit());
	    	this.setPercentage(auxParamsObj.getPercentage());
	    }
	    
		public int getPercentage() {
			return percentage;
		}
		public void setPercentage(int percentage) {
			this.percentage = percentage;
		}
		public long getLimit() {
			return limit;
		}
		public void setLimit(long limit) {
			this.limit = limit;
		}
	    	    
	}
}
