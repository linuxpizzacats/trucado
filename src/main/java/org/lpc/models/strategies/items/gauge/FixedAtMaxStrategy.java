package org.lpc.models.strategies.items.gauge;

import org.lpc.models.Item;
import org.lpc.models.enums.ItemType;
import org.lpc.models.strategies.items.Strategy;

public class FixedAtMaxStrategy extends Strategy {
	public FixedAtMaxStrategy() {
		super();
		this.setType(ItemType.GAUGE);
	}
	@Override
	public String calculate(Item item) {

		return item.getMax();
	}

}
