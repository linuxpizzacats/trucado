package org.lpc.models.dto;

public class InterfaceCreateMinimalDTO {
	private String name;
	private String highspeed;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getHighspeed() {
		return highspeed;
	}
	public void setHighspeed(String highspeed) {
		this.highspeed = highspeed;
	}
	
	
}
