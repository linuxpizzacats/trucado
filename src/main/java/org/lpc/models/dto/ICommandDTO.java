package org.lpc.models.dto;

import java.io.Serializable;

import org.lpc.models.commands.interfaces.ICommand;

public class ICommandDTO implements Serializable {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3770003675400279111L;
	private String behaviour;
	private String aux;
	
	public ICommandDTO() {
		this.setAux("");
	}
	
	public ICommandDTO(ICommand command) {
		this();
		this.setBehaviour(command.getClass().getName());
	}

	public String getBehaviour() {
		return behaviour;
	}

	public void setBehaviour(String behaviour) {
		this.behaviour = behaviour;
	}

	public String getAux() {
		return aux;
	}

	public void setAux(String aux) {
		this.aux = aux;
	}


	
}
