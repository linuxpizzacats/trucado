package org.lpc.models.dto;

import java.io.Serializable;

import org.lpc.models.Item;

public class ItemDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8748981206788226590L;
	private long id;
	private String oid;
	private String value;
	private String previousValue;
	private long tsLastQuery;
	private String max;
	private String min;
	private String type;
	private String strategyName;
	private String aux;
	
	
	public ItemDTO() {
		this.setId(0);
		this.setOid("");
		this.setValue("0");
		this.setPreviousValue("0");
		this.setTsLastQuery(System.currentTimeMillis());
		this.setMax("100");
		this.setMin("0");
		this.setAux("");
		this.setStrategyName("ZeroStrategy");		
	}
	
	public ItemDTO(Item item) {
		this();
		this.setId(item.getId());
		this.setOid(item.getOid());
		this.setValue(item.getValue());
		this.setPreviousValue(item.getPreviousValue());
		this.setTsLastQuery(item.getTsLastQuery());
		this.setMax(item.getMax());
		this.setMin(item.getMin());
		this.setType(item.getType().toString());
		this.setStrategyName(item.getStrategyName());
		this.setAux(item.getAux());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPreviousValue() {
		return previousValue;
	}

	public void setPreviousValue(String previousValue) {
		this.previousValue = previousValue;
	}

	public long getTsLastQuery() {
		return tsLastQuery;
	}

	public void setTsLastQuery(long tsLastQuery) {
		this.tsLastQuery = tsLastQuery;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStrategyName() {
		return strategyName;
	}

	public void setStrategyName(String strategyName) {
		this.strategyName = strategyName;
	}

	public String getAux() {
		return aux;
	}

	public void setAux(String aux) {
		this.aux = aux;
	}
	
}
