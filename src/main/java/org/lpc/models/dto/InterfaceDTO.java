package org.lpc.models.dto;

import java.io.Serializable;

import org.lpc.models.Interface;

public class InterfaceDTO implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2072755330322617291L;
	
	private long id;
	private ItemDTO index;
	private ItemDTO name;
	private ItemDTO alias;
	private ItemDTO operStatus;
	private ItemDTO adminStatus;
	private ItemDTO highspeed;
	private ItemDTO inDiscards;
	private ItemDTO inErrors;
	private ItemDTO hcInOctects;
	private ItemDTO outDiscards;
	private ItemDTO outErrors;
	private ItemDTO hcOutOctects;
	
	
	public InterfaceDTO() {
		this.setId(0);		
	}
	
	public InterfaceDTO(Interface interfaz) {
		this();
		this.setId(interfaz.getId());
		this.setIndex(new ItemDTO(interfaz.getIndex()));
		this.setName(new ItemDTO(interfaz.getName()));
		this.setAlias(new ItemDTO(interfaz.getAlias()));
		this.setOperStatus(new ItemDTO(interfaz.getOperStatus()));
		this.setAdminStatus(new ItemDTO(interfaz.getAdminStatus()));
		this.setHighspeed(new ItemDTO(interfaz.getHighspeed()));
		this.setInDiscards(new ItemDTO(interfaz.getInDiscards()));
		this.setInErrors(new ItemDTO(interfaz.getInErrors()));
		this.setHcInOctects(new ItemDTO(interfaz.getHcInOctects()));
		this.setOutDiscards(new ItemDTO(interfaz.getOutDiscards()));
		this.setOutErrors(new ItemDTO(interfaz.getOutErrors()));
		this.setHcOutOctects(new ItemDTO(interfaz.getOutErrors()));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ItemDTO getIndex() {
		return index;
	}

	public void setIndex(ItemDTO index) {
		this.index = index;
	}

	public ItemDTO getName() {
		return name;
	}

	public void setName(ItemDTO name) {
		this.name = name;
	}

	public ItemDTO getAlias() {
		return alias;
	}

	public void setAlias(ItemDTO alias) {
		this.alias = alias;
	}

	public ItemDTO getOperStatus() {
		return operStatus;
	}

	public void setOperStatus(ItemDTO operStatus) {
		this.operStatus = operStatus;
	}

	public ItemDTO getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(ItemDTO adminStatus) {
		this.adminStatus = adminStatus;
	}

	public ItemDTO getHighspeed() {
		return highspeed;
	}

	public void setHighspeed(ItemDTO highspeed) {
		this.highspeed = highspeed;
	}

	public ItemDTO getInDiscards() {
		return inDiscards;
	}

	public void setInDiscards(ItemDTO inDiscards) {
		this.inDiscards = inDiscards;
	}

	public ItemDTO getInErrors() {
		return inErrors;
	}

	public void setInErrors(ItemDTO inErrors) {
		this.inErrors = inErrors;
	}

	public ItemDTO getHcInOctects() {
		return hcInOctects;
	}

	public void setHcInOctects(ItemDTO hcInOctects) {
		this.hcInOctects = hcInOctects;
	}

	public ItemDTO getOutDiscards() {
		return outDiscards;
	}

	public void setOutDiscards(ItemDTO outDiscards) {
		this.outDiscards = outDiscards;
	}

	public ItemDTO getOutErrors() {
		return outErrors;
	}

	public void setOutErrors(ItemDTO outErrors) {
		this.outErrors = outErrors;
	}

	public ItemDTO getHcOutOctects() {
		return hcOutOctects;
	}

	public void setHcOutOctects(ItemDTO hcOutOctects) {
		this.hcOutOctects = hcOutOctects;
	}
	
	
	
}
