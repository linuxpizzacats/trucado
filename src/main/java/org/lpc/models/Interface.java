package org.lpc.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.lpc.models.commands.interfaces.ICommand;
import org.lpc.models.enums.ItemType;

@Entity
public class Interface {
	private long id;
	private Item index;
	private Item name;
	private Item alias;
	private Item operStatus;
	private Item adminStatus;
	private Item highspeed;
	private Item inDiscards;
	private Item inErrors;
	private Item hcInOctects;
	private Item outDiscards;
	private Item outErrors;
	private Item hcOutOctects;

	public Interface() {
		this.setId(0);
		this.setIndex(new Item(ItemType.STRING));
		this.setName(new Item(ItemType.STRING));
		this.setAlias(new Item(ItemType.STRING));
		this.setOperStatus(new Item(ItemType.GAUGE));
		this.setAdminStatus(new Item(ItemType.GAUGE));
		this.setHighspeed(new Item(ItemType.GAUGE));
		this.setInDiscards(new Item(ItemType.COUNTER));
		this.setInErrors(new Item(ItemType.COUNTER));
		this.setHcInOctects(new Item(ItemType.COUNTER));
		this.setOutDiscards(new Item(ItemType.COUNTER));
		this.setOutErrors(new Item(ItemType.COUNTER));
		this.setHcOutOctects(new Item(ItemType.COUNTER));
	}
	
	
	/*
	 * Getters
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	
	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "NAME", referencedColumnName = "ID")	
	public Item getName() {
		return name;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "ALIAS", referencedColumnName = "ID")
	public Item getAlias() {
		return alias;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "OPER_STATUS", referencedColumnName = "ID")	
	public Item getOperStatus() {
		return operStatus;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "ADMIN_STATUS", referencedColumnName = "ID")	
	public Item getAdminStatus() {
		return adminStatus;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "HIGHSPEED", referencedColumnName = "ID")	
	public Item getHighspeed() {
		return highspeed;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "IN_DISCARDS", referencedColumnName = "ID")	
	public Item getInDiscards() {
		return inDiscards;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "IN_ERRORS", referencedColumnName = "ID")	
	public Item getInErrors() {
		return inErrors;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "HC_IN_OCTECTS", referencedColumnName = "ID")	
	public Item getHcInOctects() {
		return hcInOctects;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "OUT_DISCARDS", referencedColumnName = "ID")	
	public Item getOutDiscards() {
		return outDiscards;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "OUT_ERRORS", referencedColumnName = "ID")	
	public Item getOutErrors() {
		return outErrors;
	}

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "HC_OUT_OCTECTS", referencedColumnName = "ID")	
	public Item getHcOutOctects() {
		return hcOutOctects;
	}	

	@OneToOne(optional = false, cascade = CascadeType.PERSIST, orphanRemoval = true)
	@JoinColumn(name = "INDEX", referencedColumnName = "ID")
	public Item getIndex() {
		return index;
	}
	
	/*
	 * Setters
	 */
	



	public void setIndex(Item index) {
		this.index = index;
	}


	public void setId(long id) {
		this.id = id;
	}	
	
	public void setName(Item name) {
		this.name = name;
	}

	public void setAlias(Item alias) {
		this.alias = alias;
	}

	public void setOperStatus(Item operStatus) {
		this.operStatus = operStatus;
	}

	public void setAdminStatus(Item adminStatus) {
		this.adminStatus = adminStatus;
	}

	public void setHighspeed(Item highspeed) {
		this.highspeed = highspeed;
	}

	public void setInDiscards(Item inDiscards) {
		this.inDiscards = inDiscards;
	}

	public void setInErrors(Item inErrors) {
		this.inErrors = inErrors;
	}

	public void setHcInOctects(Item hcInOctects) {
		this.hcInOctects = hcInOctects;
	}

	public void setOutDiscards(Item outDiscards) {
		this.outDiscards = outDiscards;
	}

	public void setOutErrors(Item outErrors) {
		this.outErrors = outErrors;
	}

	public void setHcOutOctects(Item hcOutOctects) {
		this.hcOutOctects = hcOutOctects;
	}
	
	/*
	 * Behaviour
	 */
	public void apply(ICommand command) {
		command.execute(this);
	}
	
	
}
