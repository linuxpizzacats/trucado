package org.lpc.models.exceptions;

import org.lpc.models.enums.ItemType;

public class WrongItemTypeForSelectedStrategyException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5716539726349197785L;

	public WrongItemTypeForSelectedStrategyException(ItemType type) {
		super("The item has a wrong type for the specified generate values strategy. Item type must be '"+type.toString()+"'");
	}
}
