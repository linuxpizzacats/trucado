package org.lpc.models.exceptions;

public class InterfaceNotFoundException extends Exception{



	/**
	 * 
	 */
	private static final long serialVersionUID = 3106499974005664879L;

	public InterfaceNotFoundException(long id) {
		super("The interface with id '"+id+"' does not exist.");
	}
}
