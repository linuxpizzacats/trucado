package org.lpc.models.exceptions;

public class BehaviourNotFoundException extends Exception{



	/**
	 * 
	 */
	private static final long serialVersionUID = 3106499974005664879L;

	public BehaviourNotFoundException() {
		super("The specified behaviour doesn't exists.");
	}
}
