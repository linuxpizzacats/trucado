package org.lpc.models.exceptions;

public class ItemNotFoundException extends Exception{



	/**
	 * 
	 */
	private static final long serialVersionUID = 4409608035208446190L;

	public ItemNotFoundException(long id) {
		super("The item with id '"+id+"' does not exist.");
	}
	public ItemNotFoundException(String oid) {
		super("The item with oid '"+oid+"' does not exist.");
	}	
}
