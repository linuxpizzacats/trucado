package org.lpc.models.exceptions;

public class ProtectedOidCreationException extends Exception{





	/**
	 * 
	 */
	private static final long serialVersionUID = -1900403216243991350L;

	public ProtectedOidCreationException() {
		super("The specified oid cannot be created.");
	}
}
