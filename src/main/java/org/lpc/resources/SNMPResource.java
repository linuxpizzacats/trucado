package org.lpc.resources;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.services.ItemService;
import org.lpc.services.exceptions.EndOfMibTreeReachedException;

@Path("/")
public class SNMPResource {
	
	@Inject
	ItemService service;
	
	@GET
	@Path("/get/{oid}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response get(@PathParam String oid) {
		try {
			return Response.status(200).entity(service.getSNMPResponse(service.get(oid))).type(MediaType.TEXT_PLAIN).build();
		}catch (ItemNotFoundException e) {
			return Response.status(204).entity("").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@GET
	@Path("/next/{oid}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response next(@PathParam String oid) {
		try {
			return Response.status(200).entity(service.getSNMPResponse(service.next(oid))).type(MediaType.TEXT_PLAIN).build();
		}catch (ItemNotFoundException e) {
			return Response.status(204).type(MediaType.TEXT_PLAIN).build();
		}catch (EndOfMibTreeReachedException e) {
			return Response.status(400).type(MediaType.TEXT_PLAIN).build();
		}
	}	
}
