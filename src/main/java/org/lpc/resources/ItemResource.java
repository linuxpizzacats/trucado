package org.lpc.resources;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.lpc.models.dto.ItemDTO;
import org.lpc.models.exceptions.ItemNotFoundException;
import org.lpc.models.exceptions.ProtectedOidCreationException;
import org.lpc.services.ItemService;
import org.lpc.services.exceptions.ItemTypeBuilderNotFoundException;

@Path("/item")
public class ItemResource {
	
	@Inject
	ItemService service;
	
	@GET
	@Path("/{id}")
	public Response get(@PathParam long id) {
		try {
			return Response.status(200).entity(service.getDTO(id)).build();
		}catch (ItemNotFoundException e) {
			return Response.status(204).build();
		}
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(ItemDTO itemDTO) {
		try {
			return Response.status(201).entity(service.getDTO(service.create(itemDTO).getId())).build();
		}catch (ItemTypeBuilderNotFoundException e) {
			return Response.status(204).build();
		} catch (ItemNotFoundException e) {
			return Response.status(404).build();
		} catch (ProtectedOidCreationException e) {
			return Response.status(403).build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam long id) {
		boolean deleted=service.delete(id);
		if(deleted) {
			return Response.status(200).entity("Item deleted").build();	
		}else {
			return Response.status(204).build();
		}
	}	
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(ItemDTO itemDTO) {
		try {
			return Response.status(200).entity(service.getDTO(service.update(itemDTO).getId())).build();
		}catch (ItemNotFoundException e) {
			try {
				return Response.status(201).entity(service.getDTO(service.create(itemDTO).getId())).build();
			} catch (ItemNotFoundException | ItemTypeBuilderNotFoundException | ProtectedOidCreationException e1) {
				return Response.status(304).build();
			}
		}
	}	

	
}
