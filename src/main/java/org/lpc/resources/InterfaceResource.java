package org.lpc.resources;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.lpc.models.dto.ICommandDTO;
import org.lpc.models.dto.InterfaceCreateMinimalDTO;
import org.lpc.models.exceptions.BehaviourNotFoundException;
import org.lpc.models.exceptions.InterfaceNotFoundException;
import org.lpc.models.exceptions.WrongItemTypeForSelectedStrategyException;
import org.lpc.services.InterfaceService;

@Path("/interface")
public class InterfaceResource {
	
	@Inject
	InterfaceService service;
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam long id) {
		try {
			return Response.status(200).entity(service.getDTO(id)).build();
		}catch (InterfaceNotFoundException e) {
			return Response.status(204).build();
		}
	}
	
	@PATCH
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response behaviour(@PathParam long id, ICommandDTO behaviour) {
		try {
			
			service.setBehaviour(service.get(id),behaviour);
			return Response.status(204).build();
		}catch (InterfaceNotFoundException | BehaviourNotFoundException e) {
			return Response.status(404).build();
		}
	}	
	
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(InterfaceCreateMinimalDTO interfaceNewMinimalDTO) {
		try {

			return Response.status(201).entity(service.getDTO(service.create(interfaceNewMinimalDTO).getId())).build();
		}catch (InterfaceNotFoundException | WrongItemTypeForSelectedStrategyException e) {
			return Response.status(409).build();
		}
	}	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list() {
		return Response.status(200).entity(service.get()).build();
	}		
}
