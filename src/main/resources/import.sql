INSERT INTO ITEM VALUES(1,'Fake SNMP device with trucado','0','0','.1.3.6.1.2.1.1.1.0','0','FixedAtStrategy','1616765377','STRING','Fake SNMP device with trucado');-- SNMPv2-MIB::sysDescr.0
INSERT INTO ITEM VALUES(2,'0','0','0','.1.3.6.1.2.1.1.3.0','0','FixedAtStrategy','1616765377','STRING','0');-- SNMPv2-MIB::sysUpTime.0
INSERT INTO ITEM VALUES(3,'trucado@gitlab','0','0','.1.3.6.1.2.1.1.4.0','0','FixedAtStrategy','1616765377','STRING','trucado@gitlab');-- SNMPv2-MIB::sysContact.0
INSERT INTO ITEM VALUES(4,'trucado-hostname','0','0','.1.3.6.1.2.1.1.5.0','0','FixedAtStrategy','1616765377','STRING','trucado-hostname');-- SNMPv2-MIB::sysName.0
INSERT INTO ITEM VALUES(5,'cloudy cloud','0','0','.1.3.6.1.2.1.1.6.0','0','FixedAtStrategy','1616765377','STRING','cloudy cloud');-- SNMPv2-MIB::sysLocation.0
INSERT INTO ITEM VALUES(6,'1','1','0','.1.3.6.1.2.1.2.2.1.1.1','0','FixedAtStrategy','1616765377','STRING','1');-- IF-MIB::ifIndex
INSERT INTO ITEM VALUES(7,'1','2','1','.1.3.6.1.2.1.2.2.1.7.1','0','FixedAtStrategy','1616765377','GAUGE','1');-- IF-MIB::ifAdminStatus
INSERT INTO ITEM VALUES(8,'1','2','1','.1.3.6.1.2.1.2.2.1.8.1','0','FixedAtStrategy','1616765377','GAUGE','1');-- IF-MIB::ifOperStatus
INSERT INTO ITEM VALUES(9,'','9223372036854775807','0','.1.3.6.1.2.1.2.2.1.13.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifInDiscards
INSERT INTO ITEM VALUES(10,'','9223372036854775807','0','.1.3.6.1.2.1.2.2.1.14.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifInErrors
INSERT INTO ITEM VALUES(11,'','9223372036854775807','0','.1.3.6.1.2.1.2.2.1.19.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifOutDiscards
INSERT INTO ITEM VALUES(12,'','9223372036854775807','0','.1.3.6.1.2.1.2.2.1.20.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifOutErrors
INSERT INTO ITEM VALUES(13,'GigaEthernet1','','','.1.3.6.1.2.1.31.1.1.1.1.1','0','FixedAtStrategy','1616765377','STRING','GigaEthernet1');-- IF-MIB::ifName
INSERT INTO ITEM VALUES(14,'','9223372036854775807','0','.1.3.6.1.2.1.31.1.1.1.6.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifHCInOctets
INSERT INTO ITEM VALUES(15,'','9223372036854775807','0','.1.3.6.1.2.1.31.1.1.1.10.1','0','ZeroStrategy','1616765377','COUNTER','0');-- IF-MIB::ifHCOutOctets
INSERT INTO ITEM VALUES(16,'1000','1000','0','.1.3.6.1.2.1.31.1.1.1.15.1','0','ZeroStrategy','1616765377','GAUGE','1000');-- IF-MIB::ifHighSpeed
INSERT INTO ITEM VALUES(17,'','','','.1.3.6.1.2.1.31.1.1.1.18.1','0','FixedAtStrategy','1616765377','STRING','');-- IF-MIB::ifAlias
INSERT INTO INTERFACE VALUES(1,7,17,14,15,16,9,10,6,13,8,11,12);-- GigaEthernet1
