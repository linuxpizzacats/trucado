;              
CREATE USER IF NOT EXISTS SA SALT 'e970f57e4ca0d5c4' HASH '37ab6e90898417863174b54d3e0ed73ab64a71994b4bdc5d6b8c292f25a28e62' ADMIN;            
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_8E762799_01FB_4370_A5C0_842550D8E596 START WITH 18 BELONGS_TO_TABLE;    
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_39721934_F236_4FA9_BF89_904B2384E1D6 START WITH 2 BELONGS_TO_TABLE;     
CREATE CACHED TABLE PUBLIC.INTERFACE(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_39721934_F236_4FA9_BF89_904B2384E1D6) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_39721934_F236_4FA9_BF89_904B2384E1D6,
    ADMIN_STATUS BIGINT NOT NULL,
    ALIAS BIGINT NOT NULL,
    HC_IN_OCTECTS BIGINT NOT NULL,
    HC_OUT_OCTECTS BIGINT NOT NULL,
    HIGHSPEED BIGINT NOT NULL,
    IN_DISCARDS BIGINT NOT NULL,
    IN_ERRORS BIGINT NOT NULL,
    INDEX BIGINT NOT NULL,
    NAME BIGINT NOT NULL,
    OPER_STATUS BIGINT NOT NULL,
    OUT_DISCARDS BIGINT NOT NULL,
    OUT_ERRORS BIGINT NOT NULL
);           
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.CONSTRAINT_C PRIMARY KEY(ID);               
-- 1 +/- SELECT COUNT(*) FROM PUBLIC.INTERFACE;
INSERT INTO PUBLIC.INTERFACE(ID, ADMIN_STATUS, ALIAS, HC_IN_OCTECTS, HC_OUT_OCTECTS, HIGHSPEED, IN_DISCARDS, IN_ERRORS, INDEX, NAME, OPER_STATUS, OUT_DISCARDS, OUT_ERRORS) VALUES
(1, 7, 17, 14, 15, 16, 9, 10, 6, 13, 8, 11, 12);            
CREATE CACHED TABLE PUBLIC.ITEM(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_8E762799_01FB_4370_A5C0_842550D8E596) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_8E762799_01FB_4370_A5C0_842550D8E596,
    AUX VARCHAR(255),
    MAX VARCHAR(255),
    MIN VARCHAR(255),
    OID VARCHAR(255),
    PREVIOUSVALUE VARCHAR(255),
    STRATEGYNAME VARCHAR(255),
    TSLASTQUERY BIGINT NOT NULL,
    TYPE VARCHAR(255),
    VALUE VARCHAR(255)
);    
ALTER TABLE PUBLIC.ITEM ADD CONSTRAINT PUBLIC.CONSTRAINT_2 PRIMARY KEY(ID);    
-- 17 +/- SELECT COUNT(*) FROM PUBLIC.ITEM;    
INSERT INTO PUBLIC.ITEM(ID, AUX, MAX, MIN, OID, PREVIOUSVALUE, STRATEGYNAME, TSLASTQUERY, TYPE, VALUE) VALUES
(1, 'Fake SNMP device with trucado', '0', '0', '.1.3.6.1.2.1.1.1.0', '0', 'FixedAtStrategy', 1616765377, 'STRING', 'Fake SNMP device with trucado'),
(2, '0', '0', '0', '.1.3.6.1.2.1.1.3.0', '0', 'FixedAtStrategy', 1616765377, 'STRING', '0'),
(3, 'trucado@gitlab', '0', '0', '.1.3.6.1.2.1.1.4.0', '0', 'FixedAtStrategy', 1616765377, 'STRING', 'trucado@gitlab'),
(4, 'trucado-hostname', '0', '0', '.1.3.6.1.2.1.1.5.0', '0', 'FixedAtStrategy', 1616765377, 'STRING', 'trucado-hostname'),
(5, 'cloudy cloud', '0', '0', '.1.3.6.1.2.1.1.6.0', '0', 'FixedAtStrategy', 1616765377, 'STRING', 'cloudy cloud'),
(6, '1', '1', '0', '.1.3.6.1.2.1.2.2.1.1.1', '0', 'FixedAtStrategy', 1616765377, 'STRING', '1'),
(7, '1', '2', '1', '.1.3.6.1.2.1.2.2.1.7.1', '0', 'FixedAtStrategy', 1616765377, 'GAUGE', '1'),
(8, '1', '2', '1', '.1.3.6.1.2.1.2.2.1.8.1', '0', 'FixedAtStrategy', 1616765377, 'GAUGE', '1'),
(9, '', '9223372036854775807', '0', '.1.3.6.1.2.1.2.2.1.13.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(10, '', '9223372036854775807', '0', '.1.3.6.1.2.1.2.2.1.14.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(11, '', '9223372036854775807', '0', '.1.3.6.1.2.1.2.2.1.19.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(12, '', '9223372036854775807', '0', '.1.3.6.1.2.1.2.2.1.20.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(13, 'GigaEthernet1', '', '', '.1.3.6.1.2.1.31.1.1.1.1.1', '0', 'FixedAtStrategy', 1616765377, 'STRING', 'GigaEthernet1'),
(14, '', '9223372036854775807', '0', '.1.3.6.1.2.1.31.1.1.1.6.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(15, '', '9223372036854775807', '0', '.1.3.6.1.2.1.31.1.1.1.10.1', '0', 'ZeroStrategy', 1616765377, 'COUNTER', '0'),
(16, '1000', '1000', '0', '.1.3.6.1.2.1.31.1.1.1.15.1', '0', 'ZeroStrategy', 1616765377, 'GAUGE', '1000'),
(17, '', '', '', '.1.3.6.1.2.1.31.1.1.1.18.1', '0', 'FixedAtStrategy', 1616765377, 'STRING', '');  
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_LWO9NGPVG9I09MBEFPUBKMUOM UNIQUE(INDEX); 
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_29DOMT13VSTV81TEJ2PTS7K5V UNIQUE(ALIAS); 
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_20JUR2DKY2483WF7XUHV0JWLY UNIQUE(HC_OUT_OCTECTS);        
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_G1I2L20GRILG873L3XEVBQ208 UNIQUE(OUT_ERRORS);            
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_N9ID2KTCOVWNXY838NH6K4COA UNIQUE(IN_DISCARDS);           
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_NWF42LB7E3BRH0WTLN6HY1FED UNIQUE(HIGHSPEED);             
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_3NKIM7WBDSHEBOF9MTBQGKXYN UNIQUE(OPER_STATUS);           
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_RNG39F4E9LNWSN09MFBIWHJ6I UNIQUE(HC_IN_OCTECTS);         
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_EH039C03OB7XL8662MAHMB7VY UNIQUE(IN_ERRORS);             
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_1MASFL4R1XJ80DJ0RSJAE2CB3 UNIQUE(ADMIN_STATUS);          
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_LEE7LLANU1FXMTYAFR36F8T0V UNIQUE(NAME);  
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.UK_S8H60E1O0O2OWAWV7RIVSL8WR UNIQUE(OUT_DISCARDS);          
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKFBEPSKLH0OUJDWD9S8N7ELO50 FOREIGN KEY(HC_OUT_OCTECTS) REFERENCES PUBLIC.ITEM(ID) NOCHECK; 
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FK7R2NCY91BLWB5VBG1LP5XOSOB FOREIGN KEY(OPER_STATUS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;    
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FK97PYP626NRMMJVIO9FD0OE1YO FOREIGN KEY(INDEX) REFERENCES PUBLIC.ITEM(ID) NOCHECK;          
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FK18QIMGQQMLFA75UBSYQOCGCTP FOREIGN KEY(IN_ERRORS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;      
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKKQLSF22PVCD8MRLNE6Q0OGJOS FOREIGN KEY(IN_DISCARDS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;    
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKGYJ1N7QGBYCVHCE8CLEB1Y3PP FOREIGN KEY(ADMIN_STATUS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;   
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKI072ACTBNB3W6XR8IGPE79NTG FOREIGN KEY(NAME) REFERENCES PUBLIC.ITEM(ID) NOCHECK;           
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FK7DYW2C74O5TTE8O29FRXQ7IGE FOREIGN KEY(ALIAS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;          
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKJ03IOYPSL3RRTXYMHQ1TWG41C FOREIGN KEY(OUT_DISCARDS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;   
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKD317A69QT3XPEHJDMET09IWE9 FOREIGN KEY(HC_IN_OCTECTS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;  
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FK9T2S1GS0DDXMYV4PPT16D1IXL FOREIGN KEY(OUT_ERRORS) REFERENCES PUBLIC.ITEM(ID) NOCHECK;     
ALTER TABLE PUBLIC.INTERFACE ADD CONSTRAINT PUBLIC.FKO0DK5J9CVGGQR9J4A44RU5VRB FOREIGN KEY(HIGHSPEED) REFERENCES PUBLIC.ITEM(ID) NOCHECK;      
